<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'status'];
    protected $withCount = ['students', 'reviews'];
    const BORRADOR = 1, REVISION = 2, PUBLICADO = 3;
    public function getRatingAttribute()
    {
        if ($this->reviews_count) {
            # code...
            return round($this->reviews->avg('rating'), 2);
        } else {
            return 5;
        }
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
    //QUERY SCOPES
    public function scopeCategory($query, $category_id)
    {
        if ($category_id) {
            # code...
            return $query->where('category_id', $category_id);
        }
    }
    public function scopeLevel($query, $level_id)
    {
        if ($level_id) {
            # code...
            return $query->where('category_id', $level_id);
        }
    }
    // RELACIONES BASE DE DATOS
    // relación uno a múchos
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
    public function requirements()
    {
        return $this->hasMany(Requirement::class);
    }
    public function goals()
    {
        return $this->hasMany(Goal::class);
    }
    public function Audiences()
    {
        return $this->hasMany(Audience::class);
    }
    public function sections()
    {
        return $this->hasMany(Section::class);
    }
    // relación uno a múchos inversa
    public function teacher()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function price()
    {
        return $this->belongsTo(Price::class);
    }
    // relación múchos a múchos
    public function students()
    {
        return $this->belongsToMany(User::class);
    }
    // RELACIONES POLIMORFICAS
    // relacion uno a uno polimorfica
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function lessons()
    {
        return $this->hasManyThrough(Lesson::class, Section::class);
    }
}
