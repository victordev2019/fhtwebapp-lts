<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    const BORRADOR = 1, PUBLICADO = 2;
    // URL AMIGABLE
    public function getRouteKeyName()
    {
        return 'slug';
    }
    // RELACIONES DB
    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
