<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    // declaración de variables
    const BORRADOR = 1, PUBLICADO = 2;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    // query scopes
    public function scopeCategoryProject($query, $category_id)
    {
        if ($category_id) {
            return $query->where('category_project_id', $category_id);
        } else {
            # code...
        }
    }
    // URL AMIGABLE
    public function getRouteKeyName()
    {
        return 'slug';
    }
    // RELACIONES DB
    public function category()
    {
        return $this->belongsTo(CategoryProject::class, 'category_project_id');
    }
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
