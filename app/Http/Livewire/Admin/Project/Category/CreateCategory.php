<?php

namespace App\Http\Livewire\Admin\Project\Category;

use App\Models\CategoryProject;
use Livewire\Component;

class CreateCategory extends Component
{
    public $createForm = ['name' => null];
    protected $rules = ['createForm.name' => 'required'];
    protected $validationAttributes = ['createForm.name' => 'Nombre'];
    public function save()
    {
        $this->validate();
        $category = new CategoryProject();
        $category->name = $this->createForm['name'];
        $category->save();
    }
    public function render()
    {
        return view('livewire.admin.project.category.create-category');
    }
}
