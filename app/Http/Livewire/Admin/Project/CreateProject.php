<?php

namespace App\Http\Livewire\Admin\Project;

use App\Models\CategoryProject;
use App\Models\Project;
use Livewire\Component;
use Illuminate\Support\Str;

class CreateProject extends Component
{
    protected $rules = [
        'category_project_id' => 'required',
        'title' => 'required',
        'slug' => 'required|unique:projects',
        'client' => 'required',
        'location' => 'required',
        'service' => 'required',
        'date' => 'required',
        'description' => 'required',
    ];
    public $categories;
    public $category_project_id;
    public $title, $slug, $description, $client, $location, $service, $date;

    public function mount()
    {
        $this->categories = CategoryProject::all();
    }
    public function updatedTitle($value)
    {
        $this->slug = Str::slug($value);
    }
    public function save()
    {
        $this->validate($this->rules);
        // dd('vlidate ok!!!');
        $project = new Project();
        $project->title = $this->title;
        $project->slug = $this->slug;
        $project->description = $this->description;
        $project->client = $this->client;
        $project->location = $this->location;
        $project->service = $this->service;
        $project->date = $this->date;
        $project->category_project_id = $this->category_project_id;

        $project->save();
        return redirect()->route('webadmin.index');
    }
    public function render()
    {
        return view('livewire.admin.project.create-project')->layout('layouts.webadmin');
    }
}
