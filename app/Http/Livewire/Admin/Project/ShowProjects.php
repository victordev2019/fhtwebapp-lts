<?php

namespace App\Http\Livewire\Admin\Project;

use App\Models\Project;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class ShowProjects extends Component
{
    use WithPagination;
    public $search;
    protected $listeners = ['delete'];
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function delete($id)
    {
        $project = Project::find($id);
        $images = $project->images;
        foreach ($images as $image) {
            Storage::disk($image->disk)->delete($image->url);
            $image->delete();
        }
        $project->delete();
        return redirect()->route('webadmin.index');
    }
    public function render()
    {
        $projects = Project::where('title', 'like', '%' . $this->search . '%')->orderBy('id', 'desc')->paginate(10);
        return view('livewire.admin.project.show-projects', compact('projects'))->layout('layouts.webadmin');
    }
}
