<?php

namespace App\Http\Livewire\Admin\Project;

use App\Models\CategoryProject;
use App\Models\Image;
use App\Models\Project;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Str;

class EditProject extends Component
{
    public $project, $categories;
    public $category_project_id;
    protected $rules = [
        'project.category_project_id' => 'required',
        'project.title' => 'required',
        'project.slug' => 'required|unique:projects,slug',
        'project.client' => 'required',
        'project.location' => 'required',
        'project.service' => 'required',
        'project.date' => 'required',
        'project.description' => 'required',
    ];
    protected $listeners = ['delete'];
    public function mount(Project $project)
    {
        $this->project = $project;
        $this->categories = CategoryProject::all();
        $this->category_project_id = $project->category->id;
    }
    public function updatedProjectTitle($value)
    {
        $this->project->slug = Str::slug($value);
    }
    public function deleteimage(Image $image)
    {
        Storage::disk('local')->delete($image->url);
        $image->delete();
        $this->project = $this->project->fresh();
        // dd($image);
    }
    public function save()
    {
        $rules = $this->rules;
        $rules['project.slug'] = 'required|unique:projects,slug,' . $this->project->id;
        $this->validate($rules);
        // dd('validate ok!!!');
        // $this->project->slug = $this->project->slug;
        $this->project->save();
        $this->emit('saved');
        return redirect()->route('webadmin.index');
    }
    public function delete()
    {
        // dd('Eliminando!!!');
        // $images = $this->project->images;
        // foreach ($images as $image) {
        //     Storage::disk($image->disk)->delete($image->url);
        //     $image->delete();
        // }
        // $this->project->delete();
        return redirect()->route('webadmin.projects.create');
    }
    public function render()
    {
        return view('livewire.admin.project.edit-project')->layout('layouts.webadmin');
    }
}
