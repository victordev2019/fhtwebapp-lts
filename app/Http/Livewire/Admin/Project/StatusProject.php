<?php

namespace App\Http\Livewire\Admin\Project;

use Livewire\Component;

class StatusProject extends Component
{
    public $project, $status;
    public function mount()
    {
        $this->status = $this->project->status;
        // dd($this->status);
    }
    public function save()
    {
        $this->project->status = $this->status;
        $this->project->save();
        $this->emit('saved');
    }
    public function render()
    {
        return view('livewire.admin.project.status-project');
    }
}
