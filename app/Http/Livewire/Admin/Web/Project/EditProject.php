<?php

namespace App\Http\Livewire\Admin\Web\Project;

use App\Models\CategoryProject;
use App\Models\Image;
use App\Models\Project;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Str;

class EditProject extends Component
{
    public $project, $categories;
    public $title;
    public $category_project_id;
    protected $rules = [
        'project.title' => 'required',
        'project.category_project_id' => 'required',
        'project.slug' => 'required|unique:projects,slug',
        'project.description' => 'required',
        'project.client' => 'required',
        'project.location' => 'required',
        'project.service' => 'required',
        'project.date' => 'required',
        // 'project.image' => 'required|image|max:5120'
        // 'status' => 'required',
    ];
    protected $listeners = ['refreshImage'];
    public function mount(Project $project)
    {
        $this->project = $project;
        // $this->title = $project->title;
        $this->categories = CategoryProject::all();
        $this->category_project_id = $project->category->id;
    }
    public function render()
    {
        return view('livewire.admin.web.project.edit-project');
    }
    // vat code
    public function updatedProjectTitle($value)
    {
        // return dd('slug update!!!');
        $this->project->slug = Str::slug($value);
    }
    public function save()
    {
        $rules = $this->rules;
        $rules['project.slug'] = 'required|unique:projects,slug,' . $this->project->id;
        $this->validate($rules);
        // $this->project->slug = $this->project->slug;
        $this->project->save();
        return redirect()->route('webadmin.project.index')->with('info', 'Proyecto actualizado con éxito.');
    }
    public function deleteImage(Image $image)
    {
        Storage::disk($image->disk)->delete($image->url);
        $image->delete();
        $this->project = $this->project->fresh();
    }
    public function refreshImage()
    {
        $this->project = $this->project->fresh();
    }
}
