<?php

namespace App\Http\Livewire\Admin\Web\Project;

use Livewire\Component;

class StatusProject extends Component
{
    public $project, $status;
    public $msg;
    public function mount()
    {
        $this->status = $this->project->status;
        $this->msg = false;
    }
    public function render()
    {
        return view('livewire.admin.web.project.status-project');
    }
    // methods vat
    public function save()
    {
        $this->project->status = $this->status;
        $this->project->save();
        $this->msg = true;
    }
}
