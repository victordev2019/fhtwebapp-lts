<?php

namespace App\Http\Livewire\Admin\Web\Project;

use App\Models\CategoryProject;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryTable extends Component
{
    use WithPagination;
    protected $listeners = ['store', 'destroy', 'update'];
    // protected $rules = [
    //     'name' => 'required'
    // ];
    public $search, $name;
    public function mount()
    {
        // $this->categories = CategoryProject::paginate(8);
    }
    public function render()
    {
        $categories = CategoryProject::where('name', 'like', '%' . $this->search . '%')
            // ->where('category_project_id', $this->select)
            ->orderBy('id', 'desc')
            ->paginate(8);
        return view('livewire.admin.web.project.category-table', compact('categories'));
    }
    public function store($name)
    {
        if ($name) {
            # code...
            $category = new CategoryProject();
            $category->name = $name;
            $category->save();
            $category->fresh();
        }
        // return dd('store category!!!' . $name);
    }
    public function destroy($id)
    {
        $category = CategoryProject::find($id);
        $category->delete();
        $category->fresh();
        // return dd('eliminando ' . $category->name);
    }
    public function update($id, $name)
    {
        if ($name) {
            # code...
            $category = CategoryProject::find($id);
            $category->name = $name;
            $category->save();
            $category->fresh();
        }
        // return dd('Editando...' . $category->id);
    }
    public function clear_page()
    {
        $this->resetPage();
    }
}
