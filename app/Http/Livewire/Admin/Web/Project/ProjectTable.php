<?php

namespace App\Http\Livewire\Admin\Web\Project;

use App\Models\CategoryProject;
use App\Models\Project;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class ProjectTable extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['destroy'];
    public $search, $select;
    public function render()
    {
        $projects = Project::where('title', 'like', '%' . $this->search . '%')
            // ->where('category_project_id', $this->select)
            ->orderBy('id', 'desc')
            ->paginate(8);
        $categories = CategoryProject::all();
        return view('livewire.admin.web.project.project-table', compact('projects', 'categories'));
    }
    public function clear_page()
    {
        $this->resetPage();
    }
    public function destroy($id)
    {
        $project = Project::find($id);
        foreach ($project->images as $image) {
            Storage::disk($image->disk)->delete($image->url);
            $image->delete();
        }
        $project->delete();
        $project->fresh();
        // return dd('Eliminando ok!!!' . $id);
    }
}
