<?php

namespace App\Http\Livewire\Admin\Web\Project;

use App\Models\CategoryProject;
use App\Models\Project;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class CreateProject extends Component
{
    use WithFileUploads;
    protected $listeners = ['addCategory'];
    public $categories, $category_id = '', $new_cat = '';
    public $title, $slug, $description, $client, $location, $service, $date, $image;
    public $isUpload = false;
    protected $rules = [
        'title' => 'required',
        'category_id' => 'required',
        'slug' => 'required|unique:projects',
        'description' => 'required',
        'client' => 'required',
        'location' => 'required',
        'service' => 'required',
        'date' => 'required',
        'image' => 'required|image|max:5120'
        // 'status' => 'required',
    ];
    public function mount()
    {
        $this->categories = CategoryProject::all();
    }
    public function render()
    {
        return view('livewire.admin.web.project.create-project');
    }
    // methods
    public function updatedTitle($value)
    {
        $this->slug = Str::slug($value);
    }
    public function updatedImage($value)
    {
        // $rules = $this->rules;
        $rule = ['image' => 'required|image|max:5120'];
        if ($this->validate($rule)) {
            $this->isUpload = true;
        } else {
            $this->isUpload = false;
        }
    }
    public function save()
    {
        $this->validate();
        $project = new Project();
        $project->title = $this->title;
        $project->slug = $this->slug;
        $project->description = $this->description;
        $project->client = $this->client;
        $project->location = $this->location;
        $project->service = $this->service;
        $project->date = $this->date;
        $project->category_project_id = $this->category_id;

        $project->save();

        $url = Storage::disk(config('global.storage'))->put('www/projects', $this->image, 'public');
        $project->images()->create([
            'url' => $url,
            'disk' => config('global.storage')
        ]);
        // return dd($url);
        return redirect()->route('webadmin.project.index')->with('info', 'Proyecto creado satisfactoriamente.');
        // return $this->image;
    }
    public function addCategory($category)
    {
        // $this->validate([
        //     'new_cat' => 'required'
        // ]);
        if ($category) {
            $cat = new CategoryProject();
            $cat->name = $category;
            $cat->save();
            $this->categories = CategoryProject::all();
        }
    }
}
