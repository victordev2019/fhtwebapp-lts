<?php

namespace App\Http\Livewire\Admin\Web\News;

use Livewire\Component;

class NewsTable extends Component
{
    public $news;
    public function render()
    {
        return view('livewire.admin.web.news.news-table');
    }
}
