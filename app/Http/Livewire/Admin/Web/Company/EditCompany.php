<?php

namespace App\Http\Livewire\Admin\Web\Company;

use App\Models\Company;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class EditCompany extends Component
{
    use WithFileUploads;
    public $company, $url, $status;
    public $image = null;
    public $isUpload = false;
    protected $rules = [
        'company.name' => 'required',
        'company.slug' => 'required',
    ];
    public function mount(Company $company)
    {
        $this->url = $company->url;
        $this->status = $company->status;
    }
    public function render()
    {
        // return dd($this->company->url);
        return view('livewire.admin.web.company.edit-company');
    }
    // vat code
    public function updatedCompanyName($value)
    {
        // return dd('slug update!!!');
        $this->company->slug = Str::slug($value);
    }
    public function updatedImage($value)
    {
        $rule = ['image' => 'required|image|max:3072'];
        if ($this->validate($rule)) {
            # code...
            $this->isUpload = true;
        } else {
            # code...
            $this->isUpload = false;
        }

        // return dd('Image change...');
    }
    public function save()
    {
        $rules = $this->rules;
        $rules['company.slug'] = 'required|unique:companies,slug,' . $this->company->id;
        $this->validate($rules);
        $this->company->url = $this->url;
        $this->company->status = $this->status;
        $this->company->save();

        if ($this->isUpload && $this->image) {
            foreach ($this->company->image as $image) {
                // return dd($image->url);
                Storage::disk($image->disk)->delete($image->url);
                $image->delete();
            }
            $url = Storage::disk(config('global.storage'))->put('www/companies', $this->image, 'public');
            $this->company->image()->create([
                'url' => $url,
                'disk' => config('global.storage')
            ]);
        }
        return redirect()->route('webadmin.company.index')->with('info', 'Información actualizada con éxito.');
    }
}
