<?php

namespace App\Http\Livewire\Admin\Web\Company;

use App\Models\Company;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;

class CompanyTable extends Component
{
    use WithPagination;
    public $search;
    protected $listeners = ['destroy'];

    public function render()
    {
        $companies = Company::where('name', 'like', '%' . $this->search . '%')
            // ->where('category_project_id', $this->select)
            ->orderBy('id', 'desc')
            ->paginate(8);
        return view('livewire.admin.web.company.company-table', compact('companies'));
    }
    public function clear_page()
    {
        $this->resetPage();
    }
    public function destroy($id)
    {
        // return dd('Eliminando...');
        $company = Company::find($id);
        foreach ($company->image as $image) {
            Storage::disk($image->disk)->delete($image->url);
            $image->delete();
        }
        $company->delete();
        $company->fresh();
        // return dd('Eliminando ok!!!' . $id);
    }
}
