<?php

namespace App\Http\Livewire\Admin\Web\Company;

use App\Models\Company;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;

class CreateCompany extends Component
{
    use WithFileUploads;
    public $name, $slug, $status, $url, $image;
    public $isUpload = false;
    protected $rules = [
        'name' => 'required',
        'slug' => 'required',
        'image' => 'required|image|max:3072'
    ];
    public function render()
    {
        return view('livewire.admin.web.company.create-company');
    }
    // methods
    public function updatedName($value)
    {
        $this->slug = Str::slug($value);
    }
    public function updatedImage()
    {
        // $rules = $this->rules;
        $rule = ['image' => 'required|image|max:3072'];
        if ($this->validate($rule)) {
            $this->isUpload = true;
        } else {
            $this->isUpload = false;
        }
    }
    public function save()
    {
        $this->validate();
        $company = new Company();
        $company->name = $this->name;
        $company->slug = $this->slug;
        $company->url = $this->url;

        $company->save();

        $url = Storage::disk(config('global.storage'))->put('www/companies', $this->image, 'public');
        $company->image()->create([
            'url' => $url,
            'disk' => config('global.storage')
        ]);
        return redirect()->route('webadmin.company.index')->with('info', 'La empresa se ha creado con éxito.');
    }
}
