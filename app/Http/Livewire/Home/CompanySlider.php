<?php

namespace App\Http\Livewire\Home;

use App\Models\Company;
use Livewire\Component;

class CompanySlider extends Component
{
    public function render()
    {
        $companies = Company::all();
        return view('livewire.home.company-slider', compact('companies'));
    }
}
