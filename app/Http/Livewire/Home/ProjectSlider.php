<?php

namespace App\Http\Livewire\Home;

use App\Models\CategoryProject;
use App\Models\Project;
use Livewire\Component;

class ProjectSlider extends Component
{
    // public $category_id = 2;
    public $projects = [];
    public function loadPosts()
    {
        $this->projects = Project::where('status', 2)
            ->take(10)
            ->orderBy('id', 'desc')
            ->get();
        $this->emit('glider');
    }
    public function render()
    {
        $categories = CategoryProject::all();
        return view('livewire.home.project-slider', compact('categories'));
    }
}
