<?php

namespace App\Http\Livewire\Project;

use Livewire\Component;

class CategoryProjects extends Component
{
    public $category;
    public $projects = [];
    public function loadPost()
    {
        // $this->projects = $this->category->projects()->where('status', 2)->get();
        $this->projects = $this->category->projects;
        $this->emit('glider', $this->category->id);
    }
    public function render()
    {
        return view('livewire.project.category-projects');
    }
}
