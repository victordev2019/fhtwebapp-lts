<?php

namespace App\Http\Livewire;

use App\Models\Project;
use Livewire\Component;

class CategoryProjects extends Component
{
    public $categories;
    public $projects = [];
    public $category_id;
    public function render()
    {
        $this->projects = Project::where('status', 2)
            ->categoryProject($this->category_id)
            ->latest('id')->get();
        $this->emit('glider');
        return view('livewire.category-projects');
    }
    public function resetFilters()
    {
        $this->reset(['category_id']);
    }
    public function loadProjects()
    {
        $this->projects = Project::where('status', 2)
            ->categoryProject($this->category_id)
            ->latest('id')->get();
        $this->emit('glider');
    }
}
