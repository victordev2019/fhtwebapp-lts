<?php

namespace App\Http\Controllers;

use App\Models\CategoryProject;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    public function __invoke()
    {
        $categories = CategoryProject::all();
        return view('proyectos', compact('categories'));
    }
}
