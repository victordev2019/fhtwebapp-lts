<?php

namespace App\Http\Controllers;

use App\Models\CategoryProject;
use App\Models\Course;
use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke()
    {
        // $projects = Project::all();
        $categories = CategoryProject::all();
        $courses = Course::where('status', 3)->latest('id')->get()->take(12);
        // return $courses;
        // return Course::find(2)->rating;
        return view('welcome', compact('courses', 'categories'));
    }
}
