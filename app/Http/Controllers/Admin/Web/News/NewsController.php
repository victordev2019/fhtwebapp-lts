<?php

namespace App\Http\Controllers\Admin\Web\News;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::all();
        return view('admin.web.news.index', compact('news'));
    }
}
