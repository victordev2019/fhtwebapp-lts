<?php

namespace App\Http\Controllers\Admin\Web\Company;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use Livewire\WithPagination;

class CompanyController extends Controller
{
    use WithPagination;
    public function index()
    {
        $companies = Company::paginate(8);
        return view('admin.web.company.index', compact('companies'));
    }
    public function create()
    {
        return view('admin.web.company.create');
    }
    public function edit(Company $company)
    {
        return view('admin.web.company.edit', compact('company'));
    }
}
