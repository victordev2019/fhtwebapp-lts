<?php

namespace App\Http\Controllers\Admin\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryProjectController extends Controller
{
    public function index()
    {
        return view('admin.web.categories-project.index');
    }
}
