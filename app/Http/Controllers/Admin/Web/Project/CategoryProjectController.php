<?php

namespace App\Http\Controllers\Admin\Web\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryProjectController extends Controller
{
    public function index()
    {
        return view('admin.web.project.category');
    }
}
