<?php

namespace App\Http\Controllers\Admin\Web\Project;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public function index()
    {
        $projects = Project::paginate(10);
        return view('admin.web.project.index', compact('projects'));
    }
    public function create()
    {
        return view('admin.web.project.create');
    }
    public function edit(Project $project)
    {
        return view('admin.web.project.edit', compact('project'));
    }
    public function files(Project $project, Request $request)
    {
        $request->validate([
            'file' => 'required|image|max:5120'
        ]);
        // $url = Storage::put('public/projects', $request->file('file'));
        $url = Storage::disk(config('global.storage'))->put('www/projects', $request->file, 'public');
        $project->images()->create([
            'url' => $url,
            'disk' => config('global.storage')
        ]);
    }
}
