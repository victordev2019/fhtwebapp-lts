<?php

namespace App\Http\Controllers\Admin\Project;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    public function files(Project $project, Request $request)
    {
        $request->validate([
            'file' => 'required|image|max:4096'
        ]);
        // $url = Storage::put('public/projects', $request->file('file'));
        $url = Storage::disk('s3')->put('www/projects', $request->file, 'public');
        $project->images()->create([
            'url' => $url,
            'disk' => 's3'
        ]);
    }
}
