<div wire:init='loadPosts'>
    {{-- filtros --}}
    <div class="text-center text-lg font-FuturaStdLight py-8">
        {{-- <button class="mx-4 capitalize hover:text-brand_primary-400 active:text-brand_primary-400"
            wire:click='resetFilters'>Todos</button> --}}
        @foreach ($categories as $category)
            <a href="{{ route('proyectos') }}"
                class="mx-4 capitalize hover:text-brand_primary-400 active:text-brand_primary-400">{{ $category->name }}</a>
        @endforeach
    </div>
    {{-- <p>Category ID: {{ $category_id }}</p> --}}
    {{-- slider proyectos --}}
    @if (count($projects))
        <div class="glider-contain">
            <ul class="glider text-black">
                @foreach ($projects as $project)
                    <li class="{{ $loop->last ? '' : 'sm:mr-3' }}">
                        <article class="h-full w-full bg-bg_brand-600 text-white">
                            <figure>
                                <a href="{{ route('project.show', $project) }}">
                                    <img class="hover:grayscale object-cover object-center w-full"
                                        src="{{ Storage::disk($project->images->first()->disk)->url($project->images->first()->url) }}"
                                        alt="">
                                </a>
                            </figure>
                            <h1 class="text-xl font-FuturaStdLight text-center px-3 py-4">
                                {{ Str::limit($project->title, 45, '...') }}</h1>
                        </article>
                    </li>
                @endforeach
            </ul>

            <button aria-label="Previous" class="glider-prev"><i
                    class="fas fa-angle-left text-4xl text-brand_primary-400"></i></button>
            <button aria-label="Next" class="glider-next"><i
                    class="fas fa-angle-right text-4xl text-brand_primary-400"></i></button>
            <div role="tablist" class="dots"></div>
        </div>
    @else
        <div class="fa-3x text-brand_primary-400 flex items-center justify-center h-56">
            <i class="fas fa-spinner fa-pulse"></i>
        </div>
    @endif
</div>
