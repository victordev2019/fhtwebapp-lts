<div class="container">
    {{-- <div class="slider">
        <div class="slider__track">
            @foreach ($companies as $company)
                <div class="slider__track--slide">
                    @foreach ($company->image as $image)
                        <img src="{{ Storage::disk($image->disk)->url($image->url) }}" alt="">
                    @endforeach
                </div>
            @endforeach
        </div>
    </div> --}}
    {{-- <div class="carrousel">
        <div class="carrousel__content">
            @foreach ($companies as $company)
                <figure>
                    @foreach ($company->image as $image)
                        <img src="{{ Storage::disk($image->disk)->url($image->url) }}" alt="">
                    @endforeach
                </figure>
            @endforeach
        </div>
    </div> --}}
    <div class="carrusel">
        <div class="carrusel-items grayscale">
            @foreach ($companies as $company)
                <section class="carrusel-item">
                    <a href="{{ $company->url }}" target="_blank">
                        @foreach ($company->image as $image)
                            <img src="{{ Storage::disk($image->disk)->url($image->url) }}" alt="">
                        @endforeach
                    </a>
                </section>
            @endforeach
        </div>
    </div>
</div>
