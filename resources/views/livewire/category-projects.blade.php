<div>
    {{-- filtros --}}
    <div class="text-center text-lg font-FuturaStdLight py-8">
        <button class="mx-4 capitalize hover:text-brand_primary-400 active:text-brand_primary-400"
            wire:click='resetFilters'>Todos</button>
        @foreach ($categories as $category)
            <button href="" class="mx-4 capitalize hover:text-brand_primary-400 active:text-brand_primary-400"
                wire:click="$set('category_id',{{ $category->id }})">{{ $category->name }}</button>
        @endforeach
    </div>
    <p>Category ID: {{ $category_id }}</p>
    {{-- slider projects --}}
    <div wire:init='loadProjects' class="glider-contain">
        <ul class="glider">
            @foreach ($projects as $project)
                <li class="{{ $loop->last ? '' : 'mx-4' }}">
                    <article class="bg-bg_brand-600 h-full text-white">
                        <figure class="text-center hover:grayscale">
                            <a href="" class="">
                                <img class="object-cover object-center"
                                    src="{{ Storage::url($project->images->first()->url) }}" alt="" srcset="">
                            </a>
                            <caption>
                                <h1 class="font-FuturaStdLight text-xl py-2 px-4">
                                    {{ Str::limit($project->title, 50, '...') }}</h1>
                                <p>{{ $project->category_project_id }}</p>
                            </caption>
                        </figure>
                    </article>
                </li>
            @endforeach
        </ul>

        <button aria-label="Previous" class="glider-prev"><i
                class="fas fa-angle-left text-brand_primary-400 text-4xl"></i></button>
        <button aria-label="Next" class="glider-next"><i
                class="fas fa-angle-right text-4xl text-brand_primary-400"></i></button>
        <div role="tablist" class="dots"></div>
    </div>
</div>
