<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <div class="d-flex">
                <input wire:keydown='clear_page' wire:model='search' class="form-control" type="text"
                    placeholder="Escriba el nombre de la noticia...">
                {{-- <select wire:model='select' class="form-control w-25" name="" id="">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select> --}}
                {{-- <p>{{ $select }}</p> --}}
                <a class="btn btn-primary flex-shrink-0 ml-2" href="{{ route('webadmin.news.create') }}"><i
                        class="fas fa-plus-circle"></i><span class="d-none d-lg-inline ml-2">Crear
                        Noticia</span></a>
            </div>
        </div>
        <div class="card-body">
            @if ($news->count())
                <x-admin.web.table>
                    <x-slot name='head'>
                        <th scope="col">#</th>
                        <th scope="col">Noticia</th>
                        <th scope="col">url</th>
                        <th scope="col">Estado</th>
                        <th colspan="2">Opciones</th>
                    </x-slot>
                    <x-slot name='data'>
                        <tr>
                            @foreach ($news as $new)
                        <tr>
                            <th scope="row">{{ $new->id }}</th>
                            <td>{{ $new->name }}</td>
                            <td>{{ $new->url }}</td>
                            <td>
                                @if ($new->status == 1)
                                    <span class="badge badge-pill badge-danger">Borrador</span>
                                @else
                                    <span class="badge badge-pill badge-success">Publicado</span>
                                @endif
                            </td>
                            <td width='10px'><a href="{{ route('webadmin.news.edit', $new) }}" class="btn btn-info"><i
                                        class="fa fa-edit"></i></a>
                            </td>
                            <td width='10px'><button wire:click='$emit("delete",{{ $new->id }})'
                                    class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                        </tr>
            @endforeach
            </tr>
            </x-slot>
            </x-admin.web.table>
        @else
            <div class="alert alert-info" role="alert">
                <strong>Información!</strong> No se han encontrado registros en la base de datos.
            </div>
            @endif
        </div>

        {{-- <div class="card-body">
            @if ($news->count())
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead class="bg-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">url</th>
                                <th scope="col">Estado</th>
                                <th colspan="2">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($news as $new)
                                <tr>
                                    <th scope="row">{{ $new->id }}</th>
                                    <td>{{ $new->name }}</td>
                                    <td>{{ $new->url }}</td>
                                    <td>
                                        @if ($new->status == 1)
                                            <span class="badge badge-pill badge-danger">Borrador</span>
                                        @else
                                            <span class="badge badge-pill badge-success">Publicado</span>
                                        @endif
                                    </td>
                                    <td width='10px'><a href="{{ route('webadmin.new.edit', $new) }}"
                                            class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td width='10px'><button wire:click='$emit("delete",{{ $new->id }})'
                                            class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-info" role="alert">
                    <strong>Información!</strong> No se han encontrado registros en la base de datos.
                </div>
            @endif
        </div> --}}
        {{-- <div class="card-footer m-auto">
            {{ $news->links() }}
        </div> --}}
    </div>
</div>
