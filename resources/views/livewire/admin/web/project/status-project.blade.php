<div class="card p-3">
    <h3>Estado del Proyecto</h3>
    <div class="d-flex">
        <div class="form-check mr-3">
            <input wire:model.defer='status' class="form-check-input" type="radio" name="status" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                Borrador
            </label>
        </div>
        <div class="form-check">
            <input wire:model.defer='status' class="form-check-input" type="radio" name="status" value="2">
            <label class="form-check-label" for="flexRadioDefault2">
                Publicado
            </label>
        </div>
    </div>
    @if ($msg)
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Éxito!</strong> El estado del proyecto está actualizado.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="d-flex justify-content-end">
        <button wire:click='save' wire:loading.attr='disabled' wire:target='save'
            class="btn btn-primary">Actualizar</button>
    </div>
</div>
