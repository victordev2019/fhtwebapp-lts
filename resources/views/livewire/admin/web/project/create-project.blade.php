<div class="container">
    <div class="card p-3">
        {{-- title --}}
        <div class="form-group">
            <label for="">Título</label>
            <input wire:model='title' type="text" class="form-control" placeholder="Ingrese el título del proyecto"
                id="validationCustom01" required>
            <div class="valid-feedback">
                Looks good!
            </div>
            <x-common.form.input-error for='title' />
        </div>
        {{-- category --}}
        <div class="input-group">
            <select wire:model='category_id' class="custom-select" id="inputGroupSelect04"
                aria-label="Example select with button addon">
                <option value="" selected disabled>Seleccione categoría...</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <!-- Button trigger modal -->
            <div class="input-group-append">
                <button wire:click='$emit("guardar")' type="button" class="btn btn-primary">
                    <i class="fa fa-plus-circle"></i>
                </button>
            </div>
        </div>
        <x-common.form.input-error for='category_id' />
        {{-- slug --}}
        <div class="form-group">
            <label for="">Slug</label>
            <input wire:model='slug' type="text" disabled class="form-control" placeholder="Slug generado...">
            {{-- <x-common.form.input-error for='slug' /> --}}
        </div>
        {{-- description --}}
        {{-- {{ $description }} --}}
        <div class="form-group">
            <div wire:ignore>
                <label for="formGroupExampleInput2">Descripción</label>
                {{-- <div wire:model='description' id="editor"></div> --}}
                <textarea wire:model='description' class="form-control" rows="8" x-data x-init="ClassicEditor
                    .create($refs.miEditor, {
                        removePlugins: ['Heading'],
                        toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'link']
                    })
                    .then(function(editor) {
                        editor.model.document.on('change:data', () => {
                            @this.set('description', editor.getData());
                        })
                    })
                    .catch(error => {
                        console.error(error);
                    });" x-ref='miEditor'></textarea>
            </div>
            <x-common.form.input-error for='description' />
        </div>
        <div class="form-row">
            {{-- client --}}
            <div class="form-group col-md-6">
                <label for="">Cliente</label>
                <input wire:model='client' type="text" class="form-control" placeholder="Ingrese el cliente">
                <x-common.form.input-error for='client' />
            </div>
            {{-- location --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Ubicación</label>
                <input wire:model='location' type="text" class="form-control" placeholder="Ingrese la ubicación">
                <x-common.form.input-error for='location' />
            </div>
        </div>
        <div class="form-row">
            {{-- service --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Servicio</label>
                <input wire:model='service' type="text" class="form-control" id="formGroupExampleInput"
                    placeholder="Ingrese el tipo de servicio">
                <x-common.form.input-error for='service' />
            </div>
            {{-- date --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Fecha</label>
                <input wire:model='date' type="date" class="form-control"
                    placeholder="Ingrese el tipo de servicio">
                <x-common.form.input-error for='date' />
            </div>
        </div>
        {{-- ********** image file *********** --}}
        <div class="form-group">
            <label for="formGroupExampleInput">Portada <small>Max-Size 5Mb</small></label>
            {{-- <input wire:model='image' type="file" class="form-control-file"> --}}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input wire:model='image' type="file" accept="image/*" class="custom-file-input"
                        aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="image">Seleccione Imagen</label>
                </div>
            </div>
            <x-common.form.input-error for='image' />
            <i wire:loading wire:target='image' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i>
            @if ($isUpload)
                <div class="mt-2">
                    <img class="{{ $isUpload ? '' : 'd-none' }}" width="120px"
                        src="{{ $isUpload ? $image->temporaryUrl() : '' }}" alt="">
                    <i class="{{ $isUpload ? 'fas fa-check text-success text-xl ml-3' : '' }}"></i>
                </div>
            @endif
            @isset($image)
            @endisset
            {{-- <input wire:model='image' type="file" id="image">
            <img id="picture" src="" alt="" width="120px"> --}}
        </div>



        <!-- Modal -->
        <div class="modal" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenteredLabel">Agregar Categoría</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Categoría</label>
                                <input wire:model.defer='new_cat' type="text" class="form-control"
                                    placeholder="Example input">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button wire:click='addCategory' type="button" class="btn btn-primary"
                            data-dismiss="modal">Guardar cambios</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="d-flex justify-content-end mt-3 mb-4">
        <a href="{{ url()->previous() }}" class="btn btn-secondary mr-2">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Cancelar</a>
        <button wire:loading.attr='disabled' wire:target='save,image' wire:click='save' class="btn btn-primary">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Crear
            Proyecto</button>
    </div>
    {{-- ******** test modal ******** --}}
    {{-- {{ $categories->first() }} --}}
</div>
