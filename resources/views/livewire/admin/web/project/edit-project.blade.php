<div class="container">
    {{-- imagenes con dropzone --}}
    <div class="card p-3">
        <form wire:ignore action="{{ route('webadmin.project.files', $project) }}" method="POST" class="dropzone"
            id="my-great-dropzone"></form>
        @if ($project->images->count())
            <section class="mt-2">
                <h3>Imagenes del Proyecto</h3>
                <ul class="d-flex flex-wrap p-0">
                    @foreach ($project->images as $image)
                        <li class="position-relative list-unstyled">
                            {{-- {{ Storage::url($image->url) }} --}}
                            <button wire:click='deleteImage({{ $image->id }})' wire:loading.attr='disabled'
                                wire:target='deleteImage({{ $image->id }})'
                                class="position-absolute btn btn-sm btn-danger mx-2 my-1 py-0">x</button>
                            <img src="{{ Storage::disk($image->disk)->url($image->url) }}"
                                class="mx-1 mb-2 object-cover" width="150px" height="100px" alt="">
                        </li>
                    @endforeach
                </ul>
            </section>
        @else
        @endif
    </div>
    {{-- *********** STATUS ********* --}}
    @livewire('admin.web.project.status-project', ['project' => $project], key('status-project' . $project->id))
    {{-- ********** INFORMACIÓN GENERAL ********** --}}
    <div class="card p-3">
        {{-- title --}}
        <div class="form-group">
            <label for="">Título</label>
            <input wire:model='project.title' type="text" class="form-control"
                placeholder="Ingrese el título del proyecto" id="validationCustom01" required>
            <div class="valid-feedback">
                Looks good!
            </div>
            <x-common.form.input-error for='project.title' />
        </div>
        {{-- category --}}
        <div class="input-group">
            <select wire:model='project.category_project_id' class="custom-select" id="inputGroupSelect04"
                aria-label="Example select with button addon">
                <option value="" selected disabled>Seleccione categoría...</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <!-- Button trigger modal -->
            <div class="input-group-append">
                <button wire:click='$emit("guardar")' type="button" class="btn btn-primary">
                    <i class="fa fa-plus-circle"></i>
                </button>
            </div>
        </div>
        <x-common.form.input-error for='category_id' />
        {{-- slug --}}
        <div class="form-group">
            <label for="">Slug</label>
            <input wire:model='project.slug' type="text" disabled class="form-control"
                placeholder="Slug generado...">
            <x-common.form.input-error for='project.slug' />
        </div>
        {{-- description --}}
        {{-- {{ $description }} --}}
        <div class="form-group">
            <div wire:ignore>
                <label for="formGroupExampleInput2">Descripción</label>
                {{-- <div wire:model='description' id="editor"></div> --}}
                <textarea wire:model='project.description' class="form-control" rows="8" x-data x-init="ClassicEditor
                    .create($refs.miEditor, {
                        removePlugins: ['Heading'],
                        toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'link']
                    })
                    .then(function(editor) {
                        editor.model.document.on('change:data', () => {
                            @this.set('project.description', editor.getData());
                        })
                    })
                    .catch(error => {
                        console.error(error);
                    });"
                    x-ref='miEditor'></textarea>
            </div>
            <x-common.form.input-error for='description' />
        </div>
        <div class="form-row">
            {{-- client --}}
            <div class="form-group col-md-6">
                <label for="">Cliente</label>
                <input wire:model='project.client' type="text" class="form-control" placeholder="Ingrese el cliente">
                <x-common.form.input-error for='client' />
            </div>
            {{-- location --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Ubicación</label>
                <input wire:model='project.location' type="text" class="form-control"
                    placeholder="Ingrese la ubicación">
                <x-common.form.input-error for='location' />
            </div>
        </div>
        <div class="form-row">
            {{-- service --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Servicio</label>
                <input wire:model='project.service' type="text" class="form-control" id="formGroupExampleInput"
                    placeholder="Ingrese el tipo de servicio">
                <x-common.form.input-error for='service' />
            </div>
            {{-- date --}}
            <div class="form-group col-md-6">
                <label for="formGroupExampleInput">Fecha</label>
                <input wire:model='project.date' type="date" class="form-control"
                    placeholder="Ingrese el tipo de servicio">
                <x-common.form.input-error for='date' />
            </div>
        </div>
        {{-- ********** image file *********** --}}

    </div>
    <div class="d-flex justify-content-end mt-3 mb-4">
        <a href="{{ url()->previous() }}" class="btn btn-secondary mr-2">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Cancelar</a>
        <button wire:loading.attr='disabled' wire:target='save' wire:click='save' class="btn btn-primary">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Guardar</button>
    </div>
</div>
