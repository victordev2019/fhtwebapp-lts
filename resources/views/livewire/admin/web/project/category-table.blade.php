<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <div class="d-flex">
                <input wire:keydown='clear_page' wire:model='search' class="form-control" type="text"
                    placeholder="Escriba el nombre de la categoría...">
                {{-- <select wire:model='select' class="form-control w-25" name="" id="">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select> --}}
                {{-- <p>{{ $select }}</p> --}}
                <button wire:click='$emit("nuevo")' class="btn btn-primary flex-shrink-0 ml-2"><i
                        class="fas fa-plus-circle"></i><span class="d-none d-lg-inline ml-2">Crear
                        Categoría</span></button>
            </div>
        </div>
        <div class="card-body">
            @if ($categories->count())
                <x-admin.web.table>
                    <x-slot name='head'>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th colspan="2">Opciones</th>
                    </x-slot>
                    <x-slot name='data'>
                        @foreach ($categories as $category)
                            <tr>
                                <th scope="row">{{ $category->id }}</th>
                                <td>{{ $category->name }}</td>
                                <td width='10px'><button
                                        wire:click='$emit("edit",{{ $category->id }},"{{ $category->name }}")'
                                        class="btn btn-info"><i class="fa fa-edit"></i></button>
                                </td>
                                <td width='10px'><button wire:click='$emit("delete",{{ $category->id }})'
                                        class="btn btn-danger"><i class="fa fa-trash"></i></button></td>
                                {{-- <td><button wire:click='destroy({{ $category->id }})' class="btn btn-danger"><i
                                                class="fa fa-trash"></i></button></td> --}}
                            </tr>
                        @endforeach
                    </x-slot>
                </x-admin.web.table>
            @else
                <div class="alert alert-info" role="alert">
                    <strong>Información!</strong> No se han encontrado registros en la base de datos.
                </div>
            @endif
        </div>
        <div class="card-footer m-auto">
            {{-- {{ $categories->links() }} --}}
        </div>
    </div>
</div>
