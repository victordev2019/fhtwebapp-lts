<div class="container">
    <div class="card p-3">
        {{-- title --}}
        <div class="form-group">
            <label for="">Nombre</label>
            <input wire:model='company.name' type="text" class="form-control"
                placeholder="Ingrese el nombre de la empresa" id="validationCustom01" required>
            <x-common.form.input-error for='name' />
        </div>
        {{-- slug --}}
        <div class="form-group">
            <label for="">Slug</label>
            <input wire:model='company.slug' type="text" disabled class="form-control"
                placeholder="Slug generado...">
            {{-- <x-common.form.input-error for='slug' /> --}}
        </div>
        {{-- url enlace --}}
        <div class="form-group">
            <label for="">Enlace</label>
            <input wire:model='url' type="text" class="form-control"
                placeholder="Ingrese un link | vinculo ref. empresa">
            <x-common.form.input-error for='client' />
        </div>
        {{-- estatus company --}}
        <div class="d-flex">
            <div class="form-check mr-3">
                <input wire:model.defer='status' class="form-check-input" type="radio" name="status" value="1">
                <label class="form-check-label" for="flexRadioDefault1">
                    Borrador
                </label>
            </div>
            <div class="form-check">
                <input wire:model.defer='status' class="form-check-input" type="radio" name="status" value="2">
                <label class="form-check-label" for="flexRadioDefault2">
                    Publicado
                </label>
            </div>
        </div>
        <section class="mt-2">
            <h3>Logo</h3>
            <div class="d-flex justify-content-center">
                @foreach ($company->image as $img)
                    <img src="{{ Storage::disk($img->disk)->url($img->url) }}" class="mx-1 mb-2" width="150px"
                        alt="">
                @endforeach
            </div>
        </section>
        {{-- ********** image file *********** --}}
        <div class="form-group">
            {{-- <label for="formGroupExampleInput">Logo Actual</label> --}}
            {{-- <img src="{{ $company->image->url }}" alt=""> --}}
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Logo <small>Max-Size 3Mb</small></label>
            {{-- <input wire:model='image' type="file" class="form-control-file"> --}}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input wire:model='image' type="file" accept="image/*" class="custom-file-input"
                        aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="image">Seleccione Imagen</label>
                </div>
            </div>
            <x-common.form.input-error for='image' />
            <i wire:loading wire:target='image' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i>
            @if ($isUpload)
                <div class="mt-2">
                    <img class="{{ $isUpload ? '' : 'd-none' }}" width="120px"
                        src="{{ $isUpload ? $image->temporaryUrl() : '' }}" alt="">
                    <i class="{{ $isUpload ? 'fas fa-check text-success text-xl ml-3' : '' }}"></i>
                </div>
            @endif
            @isset($image)
            @endisset
            {{-- <input wire:model='image' type="file" id="image">
            <img id="picture" src="" alt="" width="120px"> --}}
        </div>
    </div>
    <div class="d-flex justify-content-end mt-3 mb-4">
        <a href="{{ route('webadmin.company.index') }}" class="btn btn-secondary mr-2">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Cancelar</a>
        <button wire:loading.attr='disabled' wire:target='save,image' wire:click='save' class="btn btn-primary">
            {{-- <i wire:loading wire:target='save' class="fas fa-spinner fa-pulse text-primary text-xl mt-3"></i> --}}
            Guardar</button>
    </div>

    {{-- <p>{{ config('global.storage') }}</p> --}}
    {{-- ******** test modal ******** --}}
    {{-- {{ $categories->first() }} --}}
</div>
