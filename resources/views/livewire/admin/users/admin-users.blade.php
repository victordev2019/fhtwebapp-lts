<div>
    <div class="card">
        <div class="card-header">
            <input wire:keydown="clear_page" wire:model='search' type="text" class="form-control w-100"
                placeholder="Escriba un nombre...">
        </div>
        <div class="card-body">
            @if ($users->count())
                <x-admin.web.table>
                    <x-slot name='head'>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Email</th>
                        <th scope="col"><i class="fas fa-user-lock"></i></th>
                    </x-slot>
                    <x-slot name='data'>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td width="10px">
                                    <a class="btn btn-primary" href="{{ route('admin.users.edit', $user) }}"><i
                                            class="fas fa-plus-circle"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-admin.web.table>
        </div>
        <div class="card-footer text-center">
            {{ $users->links() }}
        </div>
    @else
        <div class="card-body">
            <strong>No hay registros.</strong>
        </div>
        @endif
    </div>
</div>
