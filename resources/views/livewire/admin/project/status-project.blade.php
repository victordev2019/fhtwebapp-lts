<div class="bg-white shadow-xl rounded-lg mb-8 p-4">
    <p class="font-FuturaStdBook text-xl">Estado del proyecto</p>
    <div class="flex items-center mt-4">
        <label class="mr-4">
            <input wire:model.defer='status' type="radio" value="1" name="status">
            BORRADOR
        </label>
        <label>
            <input wire:model.defer='status' type="radio" value="2" name="status">
            PUBLICADO
        </label>
    </div>
    <div class="flex justify-end">
        <x-jet-action-message on="saved">Actualizado!!!</x-jet-action-message>
        <x-common.controls.button-primary wire:click='save' wire:loading.attr='disabled' wire:target='save'
            class="ml-auto">
            actualizar
        </x-common.controls.button-primary>
    </div>
</div>
