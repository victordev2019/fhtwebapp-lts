<div>
    <x-jet-form-section submit="save">
        <x-slot name="title">
            Crear nueva categoría
        </x-slot>
        <x-slot name="description">
            Complete lainformación necesaria para poder crear una nueva categoría.
        </x-slot>
        <x-slot name="form">
            <div class="col-span-6">
                <x-jet-label>Nombre</x-jet-label>
                <x-jet-input wire:model="createForm.name" type="text" class="w-full mt-1" />
                <x-jet-input-error for="createForm.name" />
            </div>
            <div class="col-span-6">
                <x-jet-label>Slug</x-jet-label>
                <x-jet-input type="text" class="w-full mt-1" />
            </div>
        </x-slot>
        <x-slot name="actions">
            <x-jet-button>Agregar</x-jet-button>
        </x-slot>
    </x-jet-form-section>
</div>
