<div>
    <x-slot name='header'>
        <div class="flex items-center">
            <h2 class="font-FuturaStdBook text-xl text-gray-600">Lista de Proyectos</h2>
            <x-common.controls.link-button-primary href="{{ route('webadmin.projects.create') }}"
                class="ml-auto">
                crear proyecto
            </x-common.controls.link-button-primary>
        </div>
        <x-jet-action-message on="saved">Actualizado!!!</x-jet-action-message>
    </x-slot>
    <!-- component -->
    <section class="container mx-auto p-6 font-mono">
        <x-common.table-responsive>
            <div class="px-6 py-4">
                <x-jet-input wire:model='search' type="text" placeholder="Ingrese el nombre del proyecto"
                    class="w-full" />
            </div>
            @if ($projects->count())
                <table class="min-w-full leading-normal">
                    <thead>
                        <tr>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Proyecto</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Cliente</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Ubicación</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Servicio</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Fecha</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                                Categoría</th>
                            <th
                                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider">
                            </th>
                            <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-FuturaStdBook text-gray-600 uppercase tracking-wider"
                                colspan="2">
                                Opciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                            <tr>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->title }}
                                    </p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->client }}</p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->location }}</p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->service }}</p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->date }}</p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                    <p class="text-gray-900 whitespace-no-wrap font-FuturaStdLight">
                                        {{ $project->category->name }}</p>
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">

                                    @switch($project->status)
                                        @case(1)
                                            <p
                                                class="whitespace-no-wrap font-FuturaStdLight bg-red-200 rounded-xl text-xs text-red-600 text-center p-1">
                                                Borrador</p>
                                        @break

                                        @case(2)
                                            <p
                                                class="whitespace-no-wrap font-FuturaStdLight bg-green-200 rounded-xl text-xs text-green-600 text-center p-1">
                                                Publicado</p>
                                        @break

                                        @default
                                    @endswitch
                                </td>
                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm text-center flex">
                                    <a href="{{ route('webadmin.projects.edit', $project) }}"
                                        class="bg-emerald-600 text-white p-3 rounded-full text-center hover:bg-emerald-500 mr-4"><i
                                            class="far fa-edit"></i></a>
                                </td>
                                <td class="">
                                    <x-jet-danger-button wire:click="$emit('deleteProject',{{ $project->id }})"
                                        class="rounded-full"><i class="fas fa-trash-alt"></i>
                                    </x-jet-danger-button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="px-6 py-4">
                    <h3 class="text-xl font-FuturaStdBook text-gray-600">No hay ningún registro coincidente</h3>
                </div>
            @endif
            @if ($projects->hasPages())
                <div class="px-6 py-4">
                    {{ $projects->links() }}
                </div>
            @else
            @endif
        </x-common.table-responsive>
    </section>
    @push('script')
        <script>
            Livewire.on('deleteProject', id => {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('admin.project.show-projects', 'delete', id);
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    }
                    console.log(id);
                })
            })
        </script>
    @endpush
</div>
