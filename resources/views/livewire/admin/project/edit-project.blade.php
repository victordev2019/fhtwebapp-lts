<div>
    <header class="bg-white shadow">
        <div class="container py-8">
            <div class="flex justify-between items-center">
                <h1 class="text-xl font-FuturaStdBook text-gray-600">Proyectos</h1>
                <x-jet-danger-button wire:click="$emit('deleteProject',{{ $project->id }})">Eliminar
                </x-jet-danger-button>
            </div>
        </div>
    </header>
    {{-- formulario --}}
    <div class="form-container py-8 text-gray-600">
        <h1 class="text-xl text-gray-600 font-FuturaStdBook mb-4">Actualizar información de proyecto.</h1>
        {{-- dropzone carga de imagenes --}}
        <div class="bg-white shadow-xl rounded-lg mb-4 p-4">
            <div wire:ignore class="my-4">
                <form action="{{ route('webadmin.projects.files', $project) }}" method="POST" class="dropzone"
                    id="my-awesome-dropzone"></form>
            </div>
            @if ($project->images)
                <section class="">
                    <h1 class="text-lg mb-2 font-FuturaStdBook">Imágenes del producto</h1>
                    <ul class="flex flex-wrap">
                        @foreach ($project->images as $image)
                            <li wire:key='image-{{ $image->id }}' class="relative">
                                <img class="w-32 h-20 object-cover"
                                    src="{{ Storage::disk($image->disk)->url($image->url) }}" alt="">
                                <x-jet-danger-button wire:click='deleteimage({{ $image->id }})'
                                    wire:loading.attr='disabled' wire:target='deleteimage({{ $image->id }})'
                                    class="absolute top-1 right-1 px-2 py-1">x
                                </x-jet-danger-button>
                            </li>
                        @endforeach
                    </ul>
                </section>
            @endif
        </div>
        {{-- estado del proyecto --}}
        @livewire('admin.project.status-project', ['project' => $project], key($project->id))
        {{-- datos generales --}}
        <div class="bg-white shadow-xl rounded-lg p-4">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                {{-- select categorías --}}
                <div class="mt-4">
                    <x-jet-label value="Categoria" />
                    <select class="w-full form-control" wire:model="project.category_project_id">
                        <option class="font-sans text-gray-400" value="" selected>Seleccione una categoría</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <x-jet-input-error for="project.category_project_id" />
                    {{-- <p>Category_id: {{ $category_project_id }}</p> --}}
                </div>
                {{-- Fecha --}}
                <div class="mt-4">
                    <x-jet-label value="Fecha" />
                    <x-jet-input wire:model="project.date" type="date" placeholder="Ingrese el título del proyecto"
                        class="w-full" />
                    <x-jet-input-error for="project.date" />
                </div>
            </div>
            {{-- title --}}
            <div class="my-4">
                <x-jet-label value="Título del Proyecto" />
                <x-jet-input wire:model='project.title' name="title" type="text"
                    placeholder="Ingrese el título del proyecto" class="w-full" />
                <x-jet-input-error for="project.title" />
            </div>
            {{-- slug --}}
            <div class="my-4">
                <x-jet-label value="Slug" />
                <x-jet-input wire:model='project.slug' disabled type="text"
                    placeholder="Texto generado autaomaticamente" class="bg-gray-200 w-full" />
                <x-jet-input-error for="project.slug" />
            </div>
            {{-- Cliente --}}
            <div class="my-4">
                <x-jet-label value="Cliente" />
                <x-jet-input wire:model='project.client' type="text" placeholder="Ingrese el título del proyecto"
                    class="w-full" />
                <x-jet-input-error for="project.client" />
            </div>
            {{-- Ubicación --}}
            <div class="my-4">
                <x-jet-label value="Ubicación" />
                <x-jet-input wire:model='project.location' type="text" placeholder="Ingrese el título del proyecto"
                    class="w-full" />
                <x-jet-input-error for="project.location" />
            </div>
            {{-- Servicio --}}
            <div class="my-4">
                <x-jet-label value="Servicio" />
                <x-jet-input wire:model='project.service' type="text" placeholder="Ingrese el título del proyecto"
                    class="w-full" />
                <x-jet-input-error for="project.service" />
            </div>
            <div class="my-4">
                <div wire:ignore>
                    <x-jet-label value="Descripción" />
                    <textarea wire:model='project.description' x-data x-init="ClassicEditor
                        .create($refs.txtEditor)
                        .then(function(editor) {
                            editor.model.document.on('change:data', () => {
                                @this.set('project.description', editor.getData())
                            })
                        })
                        .catch(error => {
                            console.error(error);
                        });" x-ref="txtEditor" name="" id="" cols="30"
                        rows="6" class="form-control w-full"></textarea>
                </div>
                <x-jet-input-error for="project.description" />
            </div>
            <div class="flex">
                <x-jet-action-message on="saved">Actualizado!!!</x-jet-action-message>
                <x-common.controls.button-primary wire:click='save' class="ml-auto">guardar proyecto
                </x-common.controls.button-primary>
            </div>
        </div>
    </div>
    {{-- scripts --}}
    @push('script')
        <script>
            Dropzone.options.myAwesomeDropzone = { // camelized version of the `id`
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                dictDefaultMessage: "Arrastre una imagen al recuadro",
                acceptedFiles: 'image/*',
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 4, // MB
                accept: function(file, done) {
                    if (file.name == "justinbieber.jpg") {
                        done("Naha, you don't.");
                    } else {
                        done();
                    }
                }
            };
            // console.log('dropzone ok!!!');
            Livewire.on('deleteProject', parm => {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('admin.project.edit-project', 'delete', parm);
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    }
                    console.log(parm);
                })
            })
        </script>
    @endpush
</div>
