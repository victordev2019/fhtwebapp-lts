<div class="form-container py-8 text-gray-600">
    <h1 class="text-xl text-gray-600 font-FuturaStdBook">Complete la información para crear un proyecto.</h1>
    <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
        {{-- select categorías --}}
        <div class="mt-4">
            <x-jet-label value="Categoria" />
            <select class="w-full form-control" wire:model="category_project_id">
                <option class="font-sans text-gray-400" value="" selected>Seleccione una categoría</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <x-jet-input-error for="category_project_id" />
            {{-- <p>Category_id: {{ $category_project_id }}</p> --}}
        </div>
        {{-- Fecha --}}
        <div class="mt-4">
            <x-jet-label value="Fecha" />
            <x-jet-input wire:model="date" type="date" placeholder="Ingrese el título del proyecto"
                class="w-full" />
            <x-jet-input-error for="date" />
        </div>
    </div>
    {{-- nombre --}}
    <div class="my-4">
        <x-jet-label value="Título del Proyecto" />
        <x-jet-input wire:model='title' name="title" type="text" placeholder="Ingrese el título del proyecto"
            class="w-full" />
        <x-jet-input-error for="title" />
    </div>
    {{-- slug --}}
    <div class="my-4">
        <x-jet-label value="Slug" />
        <x-jet-input wire:model='slug' disabled type="text" placeholder="Texto generado autaomaticamente"
            class="bg-gray-200 w-full" />
        <x-jet-input-error for="slug" />
    </div>
    {{-- Cliente --}}
    <div class="my-4">
        <x-jet-label value="Cliente" />
        <x-jet-input wire:model='client' type="text" placeholder="Ingrese el título del proyecto"
            class="w-full" />
        <x-jet-input-error for="client" />
    </div>
    {{-- Ubicación --}}
    <div class="my-4">
        <x-jet-label value="Ubicación" />
        <x-jet-input wire:model='location' type="text" placeholder="Ingrese el título del proyecto"
            class="w-full" />
        <x-jet-input-error for="location" />
    </div>
    {{-- Servicio --}}
    <div class="my-4">
        <x-jet-label value="Servicio" />
        <x-jet-input wire:model='service' type="text" placeholder="Ingrese el título del proyecto"
            class="w-full" />
        <x-jet-input-error for="service" />
    </div>
    <div class="my-4">
        <div wire:ignore>
            <x-jet-label value="Descripción" />
            <textarea wire:model='description' x-data x-init="ClassicEditor
                .create($refs.txtEditor)
                .then(function(editor) {
                    editor.model.document.on('change:data', () => {
                        @this.set('description', editor.getData())
                    })
                })
                .catch(error => {
                    console.error(error);
                });" x-ref="txtEditor" name="" id="" cols="30" rows="6"
                class="form-control w-full"></textarea>
        </div>
        <x-jet-input-error for="description" />
    </div>
    <div class="flex">
        <x-common.controls.button-primary wire:click='save' class="ml-auto">Agregar proyecto
        </x-common.controls.button-primary>
    </div>
</div>
