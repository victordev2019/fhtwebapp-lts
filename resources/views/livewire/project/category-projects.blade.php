<div wire:init='loadPost'>
    @if (count($projects))
        <div class="glider-contain">
            <ul class="glider-{{ $category->id }}">
                @foreach ($projects as $project)
                    <li class="bg-bg_brand-600 {{ $loop->last ? '' : 'sm:mr-3' }}">
                        <article class="h-full w-full bg-bg_brand-600 text-white hover:grayscale">
                            <figure>
                                <a href="{{ route('project.show', $project) }}">
                                    <img class="object-cover object-center"
                                        src="@if ($project->images) {{ Storage::disk($project->images->last()->disk)->url($project->images->last()->url) }} @endif"
                                        alt="">
                                </a>
                            </figure>
                            <h1 class="font-FuturaStdLight text-lg text-center p-3">
                                {{ Str::limit($project->title, 45, '...') }}</h1>
                        </article>
                    </li>
                @endforeach
            </ul>

            <button aria-label="Previous" class="glider-prev"><i
                    class="fas fa-angle-left text-4xl text-brand_primary-400"></i></button>
            <button aria-label="Next" class="glider-next"><i
                    class="fas fa-angle-right text-4xl text-brand_primary-400"></i></button>
            <div role="tablist" class="dots"></div>
        </div>
    @else
        <div class="fa-3x text-brand_primary-400 flex items-center justify-center h-56">
            <i class="fas fa-spinner fa-pulse"></i>
        </div>
    @endif
</div>
