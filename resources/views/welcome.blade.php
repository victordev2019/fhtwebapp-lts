<x-app-layout>
    @section('title', 'Home')
    {{-- HEADER PORTADA --}}
    <x-portada stylebg="{{ asset('img/home/banner.jpg') }}" />
    {{-- <div class="testbg relative">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam cupiditate praesentium possimus
            perspiciatis? Nemo, ut ipsum. Voluptas earum omnis officiis iure aliquid sapiente sint. Illo culpa voluptas
            odio modi aliquid!</p>
    </div> --}}
    {{-- PRINCIPALES --}}
    <section class="bg-bg_brand-500 text-white py-4">
        <div class="container">
            <div class="grid grid-cols-1 md:grid-cols-3 grid-flow-row auto-rows-max py-16 gap-x-4 gap-y-8">
                <div class="flex items-center">
                    <div>
                        <x-markup href="https://fhtsrl.com">cursos</x-markup>
                        <h1 class="font-FuturaStdBook text-3xl capitalize py-6">
                            capacitación profesional</h1>
                        <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                        <p class="text-lg font-thin">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eaque,
                            harum
                            quae nisi mollitia
                            consequuntur sunt ad veritatis asperiores officiis dolorum deserunt eligendi nostrum sit
                            consequatur. Error in necessitatibus sunt ut.</p>
                    </div>
                </div>
                <div class=" bg-brand_primary-400 px-8 py-12">
                    <i class="fas fa-leaf text-5xl"></i>
                    <h1 class="text-2xl capitalize font-FuturaStdBook py-4">Cuidado del medio ambiente</h1>
                    <p class="text-lg font-thin">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa quo
                        corporis
                        harum blanditiis
                        dignissimos deleniti molestias saepe ab culpa ut maxime iure fugit corrupti autem nisi ratione,
                        perspiciatis porro consequatur!</p>
                </div>
                <div class="bg-bg_brand-600 px-8 py-12">
                    <i class="fas fa-tractor text-5xl text-brand_primary-400"></i>
                    <h1 class="text-2xl capitalize font-FuturaStdBook py-4">tecnología moderna</h1>
                    <p class="text-lg font-thin">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa quo
                        corporis harum blanditiis
                        dignissimos deleniti molestias saepe ab culpa ut maxime iure fugit corrupti autem nisi ratione,
                        perspiciatis porro consequatur!</p>
                </div>
            </div>
        </div>
    </section>
    {{-- SERVICIOS --}}
    <section class="grid grid-cols-1 lg:grid-cols-2">
        {{-- banner servicios --}}
        <div class="servicios bg-cover bg-center">
            <div class="max-w-xl ml-auto py-12 mr-10 text-white relative z-10 h-full flex items-center">
                <div class="">
                    <div class="flex flex-row-reverse">
                        <x-markup class="inline text-right">explora nuestros</x-markup>
                    </div>
                    <h1 class="font-FuturaStdBook text-4xl capitalize py-6 text-right">
                        Principales Servicios</h1>
                    <div class="flex flex-row-reverse">
                        <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4 text-right">
                    </div>
                    <p class="text-lg font-thin text-right">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                        Eaque,
                        harum
                        quae nisi mollitia
                        consequuntur sunt ad veritatis asperiores officiis dolorum deserunt eligendi nostrum sit
                        consequatur. Error in necessitatibus sunt ut.</p>
                </div>
            </div>
        </div>
        {{-- aside servicios --}}
        <div class="aside-servicios bg-bg_brand-400 py-8">
            <div class="flex items-center max-w-xl ml-10 mb-4">
                <div class="icon text-6xl text-brand_primary-400 mr-8">
                    <i class="fas fa-hard-hat"></i>
                </div>
                <div class="info text-white">
                    <a href="" class="hover:text-brand_primary-400">
                        <h2 class="uppercase font-FuturaStdBook text-xl">construcción</h2>
                    </a>
                    <p class="text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fuga natus
                        excepturi voluptate
                        possimus, voluptatibus error dolorem porro mollitia, inventore ipsum numquam beatae.</p>
                </div>
            </div>
            <div class="flex items-center max-w-xl ml-10 mb-4">
                <div class="icon text-6xl text-brand_primary-400 mr-8">
                    <i class="fas fa-headset"></i>
                </div>
                <div class="info text-white">
                    <a href="" class="hover:text-brand_primary-400">
                        <h2 class="uppercase font-FuturaStdBook text-xl">consultoría</h2>
                    </a>
                    <p class="text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fuga natus
                        excepturi voluptate
                        possimus, voluptatibus error dolorem porro mollitia, inventore ipsum numquam beatae.</p>
                </div>
            </div>
            <div class="flex items-center max-w-xl ml-10 mb-4">
                <div class="icon text-6xl text-brand_primary-400 mr-5">
                    <i class="fas fa-cloud-download-alt"></i>
                </div>
                <div class="info text-white">
                    <a href="" class="hover:text-brand_primary-400">
                        <h2 class="uppercase font-FuturaStdBook text-xl">geo5</h2>
                    </a>
                    <p class="text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fuga natus
                        excepturi voluptate
                        possimus, voluptatibus error dolorem porro mollitia, inventore ipsum numquam beatae.</p>
                </div>
            </div>
            <div class="flex items-center max-w-xl ml-10 mb-4">
                <div class="icon text-6xl text-brand_primary-400 mr-5">
                    <i class="fas fa-cogs"></i>
                </div>
                <div class="info text-white">
                    <a href="" class="hover:text-brand_primary-400">
                        <h2 class="uppercase font-FuturaStdBook text-xl">soporte</h2>
                    </a>
                    <p class="text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fuga natus
                        excepturi voluptate
                        possimus, voluptatibus error dolorem porro mollitia, inventore ipsum numquam beatae.</p>
                </div>
            </div>
            <div class="flex items-center max-w-xl ml-10">
                <div class="icon text-6xl text-brand_primary-400 mr-5">
                    <i class="fas fa-graduation-cap"></i>
                </div>
                <div class="info text-white">
                    <a href="" class="hover:text-brand_primary-400">
                        <h2 class="uppercase font-FuturaStdBook text-xl">capacitación</h2>
                    </a>
                    <p class="text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi fuga natus
                        excepturi voluptate
                        possimus, voluptatibus error dolorem porro mollitia, inventore ipsum numquam beatae.</p>
                </div>
            </div>
        </div>
    </section>
    {{-- PROYECTOS --}}
    <section class="bg-bg_brand-500 py-12">
        <div class="container text-white">
            <header class="text-center">
                <x-markup>nuestro trabajo</x-markup>
                <h1 class="capitalize font-FuturaStdBook text-4xl py-8">Últimos Proyectos</h1>
                <hr style="height: 4px;border: none;width: 10%" class="bg-brand_primary-400 mb-4 mx-auto">
                <p class="w-full md:w-1/2 mx-auto">Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Reprehenderit
                    laborum aspernatur voluptatum autem ducimus rerum aperiam explicabo
                    sapiente.</p>
            </header>
            <article>

                @livewire('home.project-slider')

                <div class="md:flex py-8 justify-center items-center">
                    <h2 class="text-center text-2xl mr-8 font-FuturaStdBook mb-3 md:mb-0">Quieres iniciar tu
                        proyecto<span class="font-sans">?</span></h2>
                    <div class="w-full md:w-auto text-center">
                        <x-common.primary-link href="/">contáctanos ahora</x-common.primary-link>
                    </div>
                </div>
            </article>
        </div>
    </section>
    {{-- ASIDE --}}
    <section class="">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 text-white">
            <div class="bg-bg_brand-600 py-20 px-8 flex flex-col justify-center items-center">
                <div class="flex flex-row-reverse"></div>
                <x-markup href="https://fhtsrl.com" class="flex mr-auto">por qué elegir</x-markup>
                <h1 class="font-FuturaStdBook text-3xl capitalize py-6 mr-auto">
                    nuestra empresa</h1>
                <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4 mr-auto">
                <p class="text-lg font-thin">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eaque,
                    harum
                    quae nisi mollitia
                    consequuntur sunt ad veritatis asperiores officiis dolorum deserunt eligendi nostrum sit
                    consequatur. Error in necessitatibus sunt ut.</p>
            </div>
            <div class="bg-bg_brand-400 py-20 px-8 flex flex-col items-center justify-center">
                <div
                    class="bg-brand_primary-400 border-8 border-brand_primary-200 inline p-5 font-FuturaStdMedium font-black text-4xl">
                    01</div>
                <h1 class="font-FuturaStdBook text-3xl py-6 capitalize">Años de Experiencia</h1>
                <p class="text-lg">Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Integer adipiscing erat eget
                    risus sollicitudin pellentesque et non erat.
                    Maecenas nibh dolor, malesuada et biben a,
                    sagittis accumsan ipsum.</p>
            </div>
            <div class="bg-bg_brand-600 py-20 px-8 flex flex-col items-center justify-center">
                <div
                    class="bg-brand_primary-400 border-8 border-brand_primary-200 inline p-5 font-FuturaStdMedium font-black text-4xl">
                    02</div>
                <h1 class="font-FuturaStdBook text-3xl py-6 capitalize">trabajo multidiciplinar</h1>
                <p class="text-lg">Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Integer adipiscing erat eget
                    risus sollicitudin pellentesque et non erat.
                    Maecenas nibh dolor, malesuada et biben a,
                    sagittis accumsan ipsum.</p>
            </div>
            <div class="bg-bg_brand-400 py-20 px-8 flex flex-col items-center justify-center">
                <div
                    class="bg-brand_primary-400 border-8 border-brand_primary-200 inline p-5 font-FuturaStdMedium font-black text-4xl">
                    03</div>
                <h1 class="font-FuturaStdBook text-3xl py-6 capitalize">tecnología e innovación</h1>
                <p class="text-lg">Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Integer adipiscing erat eget
                    risus sollicitudin pellentesque et non erat.
                    Maecenas nibh dolor, malesuada et biben a,
                    sagittis accumsan ipsum.</p>
            </div>

        </div>
    </section>
    {{-- CURSOS --}}
    {{-- <section class="py-20 bg-bg_brand-500 text-white">
        <div class="flex flex-col mb-4">
            <x-markup class="mx-auto">últimos cursos</x-markup>
            <h1 class="capitalize font-FuturaStdBook text-4xl py-8 text-center">Trabajamos para seguir subiendo cursos.
            </h1>
            <hr style="height: 4px;border: none;width: 10%" class="bg-brand_primary-400 mb-4 mx-auto">
        </div>
        <div class="container grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-x-6 gap-y-8">
            @foreach ($courses as $course)
                <x-course-card :course="$course" />
            @endforeach
        </div>
    </section> --}}
    {{-- COMPANIES --}}
    <section class="bg-bg_brand-500">
        @livewire('home.company-slider')
    </section>
    {{-- JAVASCRIPT --}}
    @push('script')
        <script>
            // GLIDER
            Livewire.on('glider', function() {
                new Glider(document.querySelector('.glider'), {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    draggable: true,
                    dots: '.dots',
                    arrows: {
                        prev: '.glider-prev',
                        next: '.glider-next'
                    },
                    responsive: [{
                        // screens greater than >= 775px
                        breakpoint: 775,
                        settings: {
                            // Set to `auto` and provide item width to adjust to viewport
                            slidesToShow: 2,
                            slidesToScroll: 3,
                            itemWidth: 150,
                            duration: 0.25
                        }
                    }, {
                        // screens greater than >= 1024px
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            itemWidth: 150,
                            duration: 0.25
                        }
                    }]
                });
            });
        </script>
    @endpush
</x-app-layout>
{{-- cap 14 time 41:00 --}}
