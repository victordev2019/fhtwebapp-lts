<x-app-layout>
    @section('title', 'Contactos')
    <x-common.banner stylebg="{{ asset('img/home/banner.jpg') }}">
        <x-slot name="title">contactos</x-slot>
        contactos
    </x-common.banner>
    {{-- ADDRESS --}}
    <section class="bg-bg_brand-500 py-16">
        <div class="container text-white">
            <div class="grid grid-cols-1 md:grid-cols-2 gap-12">
                {{-- la paz --}}
                <div class="map">
                    <iframe class="mapa-google rounded-lg"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d568.645176207477!2d-68.12764054488603!3d-16.507230010725117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x915f20647f5fdc11%3A0xea272ad610a713dd!2sEdificio%20Alianza!5e0!3m2!1ses!2sbo!4v1617593056511!5m2!1ses!2sbo"
                        width="100%" height="100%" style="border:0;" allowfullscreen="true" loading="lazy"></iframe>
                </div>
                <div class="info">
                    <x-markup href="https://fhtsrl.com">direcciones</x-markup>
                    <h1 class="font-FuturaStdBook text-3xl capitalize py-4">
                        la paz</h1>
                    <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                    <div class="list">
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-map-marked-alt m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">oficinas</h2>
                                <p class="text-base">Av. 6 de Agosto. Edificio Alianza N 2190
                                    Primer Piso Oficinas 105/107/108
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-envelope-open m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">email</h2>
                                <p class="text-base">ingenieria.fht@gmail.com</p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-phone m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">teléfonos</h2>
                                <p class="text-base">+591 73725190<br>
                                    +591 76725400
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div
                                class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45 hover:bg-transparent border-2 border-brand_primary-400 hover:text-brand_primary-400">
                                <a href="https://wa.me/59176725400?text=Realice cualquier consulta aquí "
                                    target="_blank"><i class="fab fa-whatsapp m-5 -rotate-45"></i></a>
                            </div>
                            <div class="info text-white">
                                <a href="https://wa.me/59176725400?text=Realice cualquier consulta aquí "
                                    target="_blank" class="hover:text-brand_primary-400">
                                    <h2 class="capitalize font-FuturaStdBook text-2xl">whatsapp</h2>
                                </a>
                                <p class="text-base">Contáctenos directamente desde este enlace whatsapp.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- santa cruz --}}
                <div class="map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3799.737212541813!2d-63.200753582556146!3d-17.7570171!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x93f1e7f495bcd231%3A0x63cc584e6e531078!2sRolea%20Center!5e0!3m2!1ses!2sbo!4v1647619893547!5m2!1ses!2sbo"
                        width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                <div class="info">
                    <x-markup href="https://fhtsrl.com">direcciones</x-markup>
                    <h1 class="font-FuturaStdBook text-3xl capitalize py-4">
                        santa cruz</h1>
                    <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                    <div class="list">
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-map-marked-alt m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">oficinas</h2>
                                <p class="text-base">Equipetrol Norte. Edificio Rolea Center N 120
                                    Primer Piso Oficina 1A
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-envelope-open m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">email</h2>
                                <p class="text-base">ingenieria.fht@gmail.com</p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45">
                                <i class="fas fa-phone m-5 -rotate-45"></i>
                            </div>
                            <div class="info text-white">
                                <h2 class="capitalize font-FuturaStdBook text-2xl">teléfonos</h2>
                                <p class="text-base">+591 70226327<br>
                                    +591 67865550
                                </p>
                            </div>
                        </div>
                        <div class="flex items-center max-w-xl mb-4 mt-8">
                            <div
                                class="icon text-2xl bg-brand_primary-400 text-white mr-8 rounded-md rotate-45 hover:bg-transparent border-2 border-brand_primary-400 hover:text-brand_primary-400">
                                <a href="https://wa.me/59176725400?text=Realice cualquier consulta aquí "
                                    target="_blank"><i class="fab fa-whatsapp m-5 -rotate-45"></i></a>
                            </div>
                            <div class="info text-white">
                                <a href="https://wa.me/59176725400?text=Realice cualquier consulta aquí "
                                    target="_blank" class="hover:text-brand_primary-400">
                                    <h2 class="capitalize font-FuturaStdBook text-2xl">whatsapp</h2>
                                </a>
                                <p class="text-base">Contáctenos directamente desde este enlace whatsapp.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-app-layout>
