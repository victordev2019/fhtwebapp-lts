<x-app-layout>
    @section('title', 'Proyectos')
    <x-common.banner stylebg="{{ asset('img/home/banner.jpg') }}">
        <x-slot name="title">proyectos</x-slot>
        proyectos
    </x-common.banner>
    {{-- PROYECTOS --}}
    <section class="bg-bg_brand-500 py-16 text-white">
        <div class="container">
            @foreach ($categories as $category)
                <div>
                    <h1 class="text-lg uppercase font-FuturaStdBook my-3 ml-6">{{ $category->name }}</h1>
                    @livewire('project.category-projects', ['category' => $category])
                </div>
            @endforeach
        </div>
    </section>
    @push('projectjs')
        <script>
            Livewire.on('glider', function(id) {
                new Glider(document.querySelector('.glider-' + id), {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    draggable: true,
                    dots: '.glider-' + id + '~ .dots',
                    arrows: {
                        prev: '.glider-' + id + '~ .glider-prev',
                        next: '.glider-' + id + '~ .glider-next'
                    },
                    responsive: [{
                        // screens greater than >= 775px
                        breakpoint: 775,
                        settings: {
                            // Set to `auto` and provide item width to adjust to viewport
                            slidesToShow: 3,
                            slidesToScroll: 4,
                            // itemWidth: 150,
                            duration: 0.25
                        }
                    }, {
                        // screens greater than >= 1024px
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 5,
                            // itemWidth: 150,
                            duration: 0.25
                        }
                    }]
                });
            });
        </script>
    @endpush
</x-app-layout>
