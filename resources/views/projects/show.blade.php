<x-app-layout>
    <x-common.banner
        stylebg="{{ Storage::disk($project->images->first()->disk)->url($project->images->first()->url) }}">
        <x-slot name="title">{{ Str::limit($project->title, 25, '...') }}</x-slot>
        {{ Str::limit($project->title, 10, '...') }}
    </x-common.banner>
    <section class="bg-bg_brand-500">
        <div class="container text-white pt-12">
            <h2 class="text-center text-3xl pb-4">{{ $project->title }}</h2>
            {{-- slider img --}}
            <div>
                <!-- Place somewhere in the <body> of your page -->
                <div class="flexslider">
                    <ul class="slides">
                        @foreach ($project->images as $image)
                            <li data-thumb="{{ Storage::disk($image->disk)->url($image->url) }}">
                                <img src="{{ Storage::disk($image->disk)->url($image->url) }}" />
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            {{-- image detail --}}
            <div class="grid grid-cols-1 lg:grid-cols-2 gap-0 lg:gap-6">
                {{-- Detalle --}}
                <div class="mb-4">
                    <h1 class="font-FuturaStdBook text-3xl capitalize">detalle del proyecto</h1>
                    <hr style="height: 4px;border: none;width: 45%" class="bg-brand_primary-400 mt-4 mb-8">
                    <div class="border-2 border-bg_brand-400">
                        <ul class=" py-8 px-6 font-FuturaStdLight text-xl">
                            <li class="py-4 border-b-2 border-bg_brand-400"><span class="font-black">Cliente:
                                </span>{{ $project->client }}</li>
                            <li class="py-4 border-b-2 border-bg_brand-400"><span class="font-black">Ubicación:
                                </span>{{ $project->location }}</li>
                            <li class="py-4 border-b-2 border-bg_brand-400"><span class="font-black">Servicio:
                                </span>{{ $project->service }}</li>
                            <li class="py-4 border-b-2 border-bg_brand-400"><span class="font-black">Fecha:
                                </span>{{ $project->date }}
                            </li>
                            <li class="py-4"><span class="font-black">Categoría:
                                </span>{{ $project->category->name }}</li>
                        </ul>
                    </div>
                </div>
                {{-- descripción --}}
                <div class="pb-12">
                    <h1 class="font-FuturaStdBook text-3xl capitalize">descripción</h1>
                    <hr style="height: 4px;border: none;width: 45%" class="bg-brand_primary-400 mt-4 mb-8">
                    {{-- <h1 class=" font-FuturaStdBook text-3xl text-center uppercase mb-2">{{ $project->title }}</h1> --}}
                    <p>{!! $project->description !!}</p>
                </div>
            </div>
        </div>
    </section>
    @push('script')
        <script>
            // Can also be used with $(document).ready()
            $(document).ready(function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: "thumbnails"
                });
            });
        </script>
    @endpush
</x-app-layout>
