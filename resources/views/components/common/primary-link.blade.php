<a
    {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center px-4 py-3 bg-brand_primary-400 border border-transparent font-semibold text-base text-white uppercase tracking-wide font-light hover:bg-brand_primary-500 active:bg-brand_primary-600 focus:outline-none focus:border-brand_primary-600 focus:ring focus:ring-brand_primary-200 disabled:opacity-25 transition']) }}>
    {{ $slot }}
</a>
