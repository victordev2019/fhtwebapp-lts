<a
    {{ $attributes->merge(['type' => 'submit','class' =>'inline-flex items-center px-4 py-2 bg-brand_primary-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-brand_primary-300 active:bg-brand_primary-500 focus:outline-none focus:border-brand_primary-500 focus:ring focus:ring-brand_primary-100 disabled:opacity-25 transition']) }}>
    {{ $slot }}
</a>
