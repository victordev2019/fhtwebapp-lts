<button
    {{ $attributes->merge(['type' => 'submit', 'class' => 'bg-red-600 text-white p-3 rounded-full text-center hover:bg-red-500']) }}>
    {{ $slot }}
</button>
