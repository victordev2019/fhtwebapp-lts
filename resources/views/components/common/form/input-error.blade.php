@props(['for'])
@error($for)
    <div class="text-danger">
        <i class="fas fa-exclamation-triangle"></i><small class="ml-2">{{ $message }}</small>
    </div>
@enderror
