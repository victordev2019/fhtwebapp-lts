@props(['id', 'action', 'title' => 'Modal titlexx'])
<!-- Modal -->
<div class="modal" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenteredLabel">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ $slot }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button wire:click='$emit("guardar")' type="button" class="btn btn-primary">Save
                    changes</button>
            </div>
        </div>
    </div>
</div>
