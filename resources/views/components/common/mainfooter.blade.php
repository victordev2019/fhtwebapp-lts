<footer class="bg-bg_brand-600 text-white py-12">
    <div class="container">
        {{-- aside info --}}
        <div class="grid grid-cols-1 md:grid-cols-3 gap-8">
            <div>
                <h1 class="font-FuturaStdBook text-3xl capitalize py-8">sobre nosotros</h1>
                <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                <p class="text-xl mb-4">Nosotros somos Fahrenheit, una empresa especializada en Ingeniería civil y
                    arquitectura que participa
                    activamente en la industria de la construcción, consultorías, capacitación para profesionales y
                    formación académica.</p>
                <p class="text-xl">Nuestra trayectoria y crecimiento se encuentra en desarrollo desde el 2012,
                    brindando a nuestros
                    clientes resultados de puntualidad, responsabilidad, seriedad y eficiencia con más de 8 años de
                    experiencia en el mercado.</p>
            </div>
            <div>
                <h1 class="font-FuturaStdBook text-3xl capitalize py-8">enlaces rápidos</h1>
                <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                <ul>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">inicio</a>
                    </li>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">servicios</a>
                    </li>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">proyectos</a>
                    </li>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">geo5</a>
                    </li>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">acerca
                            de</a></li>
                    <li class="py-2"><a href=""
                            class="font-FuturaStdLight text-xl uppercase hover:text-brand_primary-400">contactos</a>
                    </li>
                </ul>
            </div>
            <div>
                <h1 class="font-FuturaStdBook text-3xl capitalize py-8">Presencia</h1>
                <hr style="height: 4px;border: none;width: 20%" class="bg-brand_primary-400 mb-4">
                <figure>
                    <img src="{{ asset('img/assets/mapa_bol.png') }}" alt="">
                </figure>
            </div>
        </div>
        {{-- widget cards --}}
        <div class="grid grid-cols-1 md:grid-cols-4 py-8 gap-4">
            <div class="flex items-center border-2 border-bg_brand-400 p-4">
                <div class="icon text-4xl text-brand_primary-400 mr-4">
                    <i class="fas fa-map-marker-alt"></i>
                </div>
                <div class="info text-white font-FuturaStdLight">
                    <p class="text-base"><span class="font-black">LA PAZ </span>Av. 6 de Agosto.
                        Edificio Alianza N 2190
                        <span class="font-black">SANTA CRUZ </span>Equipetrol
                        Norte. Edificio Rolea
                        Center N 120
                    </p>
                </div>
            </div>
            <div class="flex items-center border-2 border-bg_brand-400 p-4">
                <div class="icon text-4xl text-brand_primary-400 mr-4">
                    <i class="fas fa-fax"></i>
                </div>
                <div class="info text-white">
                    <h2 class="capitalize font-FuturaStdBook text-2xl mb-2">fax</h2>
                    <p class="">+515574657<br>
                        +515575860</p>
                </div>
            </div>
            <div class="flex items-center border-2 border-bg_brand-400 p-4">
                <div class="icon text-4xl text-brand_primary-400 mr-4">
                    <i class="fas fa-mobile"></i>
                </div>
                <div class="info text-white">
                    <h2 class="capitalize font-FuturaStdBook text-2xl mb-2">teléfono</h2>
                    <p class="font-FuturaStdLight">73725190 - 76725400 <span class="font-black">LP</span><br>
                        70226327 - 67865550 <span class="font-black">SC</span></p>
                </div>
            </div>
            <div class="flex items-center border-2 border-bg_brand-400 p-4">
                <div class="icon text-4xl text-brand_primary-400 mr-4">
                    <i class="fas fa-envelope"></i>
                </div>
                <div class="info text-white">
                    <h2 class="capitalize font-FuturaStdBook text-2xl mb-2">e-mail</h2>
                    <p class="font-FuturaStdLight">ingenieria.fht@gmail.com</p>
                </div>
            </div>
        </div>
        <div class="text-4xl text-center py-8">
            <a href="" class="hover:text-brand_primary-400 mx-3">
                <i class="fab fa-facebook-square"></i>
            </a>
            <a href="" class="hover:text-brand_primary-400 mx-3">
                <i class="fab fa-instagram-square"></i>
            </a>
            <a href="" class="hover:text-brand_primary-400 mx-3">
                <i class="fab fa-youtube"></i>
            </a>
            <a href="" class="hover:text-brand_primary-400 mx-3">
                <i class="fab fa-whatsapp-square"></i>
            </a>
        </div>
    </div>
    <div class="border-t-2 border-bg_brand-400">
        <div class="container text-center py-8">
            <h3 class="font-FuturaStdLight text-base tracking-wider">© 2021 <span
                    class="font-black text-brand_primary-400">FAHRENHEIT
                    S.R.L.</span> Todos los derechos reservados.
                Desarrollado
                por
                <span class="font-black text-gray-300">FAHRENHEIT SYSTEMS</span>
            </h3>
        </div>
    </div>
</footer>
