@props(['stylebg' => 'salmon'])
{{-- <header class="h-screen bg-[url('/img/home/banner.jpg')] bg-cover bg-center pt-24"> --}}
<header class="banner pt-16 bg-center bg-cover relative z-0" style="background-image: url('{{ $stylebg }}')">
    <div class="container h-full relative">
        {{-- contenido --}}
        <div class="flex items-center justify-center h-full">
            <h1 class="font-FuturaStdMedium text-4xl text-white uppercase">{{ $title }}</h1>
        </div>
        <div class="absolute left-0 bottom-0 w-full flex justify-center">
            <div class="w-full md:w-1/3">
                <div class="trapezoid flex items-center justify-center py-4">
                    <a href="/" class="text-white font-FuturaStdLight uppercase text-base"><i class="fa fa-home mr-2"></i>
                        Inicio /
                        <span>{{ $slot }}</span></a>
                </div>
            </div>
        </div>
    </div>
</header>
