<style>
    .forma {
        clip-path: polygon(12% 0%, 88% 0%, 100% 100%, 0% 100%);
    }

</style>
{{-- <a href=""
    class="bg-brand_primary-400 py-1 px-8 font-FuturaStdLight uppercase tracking-wider forma text-white mr-auto">{{ $slot }}</a> --}}
<a
    {{ $attributes->merge(['type' => 'submit','class' => 'bg-brand_primary-400 py-1 px-8 font-FuturaStdLight uppercase tracking-wider forma text-white']) }}>
    {{ $slot }}
</a>
