<div class="table-responsive">
    <table class="table table-striped">
        <thead class="bg-dark">
            <tr>
                {{ $head }}
            </tr>
        </thead>
        <tbody>
            {{ $data }}
        </tbody>
    </table>
</div>
