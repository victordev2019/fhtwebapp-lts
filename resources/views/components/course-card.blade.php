@props(['course'])
<article class="bg-bg_brand-600 text-white">
    <img src="{{ Storage::url($course->image->url) }}" alt="">
    <div class="card-body">
        <h1 class="text-white text-xl font-FuturaStdBook">
            {{ Str::limit($course->title, 30, '...') }}
            {{-- <h1 class="text-xl text-gray-700 mb-2 leading-6">{{ $course->title }} --}}
        </h1>
        <p class="text-gray-400 text-sm mb-2">Prof: {{ $course->teacher->name }}</p>
        <div class="flex">
            <ul class="flex text-sm">
                <li class="mr-1">
                    <i class="text-{{ $course->rating >= 2 ? 'yellow' : 'gray' }} fas fa-star"></i>
                </li>
                <li class="mr-1">
                    <i class="text-{{ $course->rating >= 2 ? 'yellow' : 'gray' }} fas fa-star"></i>
                </li>
                <li class="mr-1">
                    <i class="text-{{ $course->rating >= 3 ? 'yellow' : 'gray' }} fas fa-star"></i>
                </li>
                <li class="mr-1">
                    <i class="text-{{ $course->rating >= 4 ? 'yellow' : 'gray' }} fas fa-star"></i>
                </li>
                <li class="mr-1">
                    <i class="text-{{ $course->rating == 5 ? 'yellow' : 'gray' }} fas fa-star"></i>
                </li>
            </ul>
            <p class="text-sm text-gray-400 ml-auto">
                <i class="fas fa-users"></i>
                ({{ $course->students_count }})
            </p>
        </div>
        <a href="{{ route('courses.show', $course) }}" class="btn btn-primary btn-block mt-4 mr-6">
            Más información
        </a>
    </div>
    <p class="p-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit laborum aspernatur
        voluptatum autem
        ducimus rerum aperiam explicabo sapiente. Exercitationem quidem excepturi ullam animi repellendus cupiditate
        maxime deserunt autem. Eaque, reprehenderit?</p>
</article>
