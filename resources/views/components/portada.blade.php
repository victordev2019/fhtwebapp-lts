@props(['stylebg' => 'salmon'])
{{-- <header class="h-screen bg-[url('/img/home/banner.jpg')] bg-cover bg-center pt-24"> --}}
<header class="portada pt-20 bg-center bg-cover relative z-0" style="background-image: url('{{ $stylebg }}')">
    <div class="container h-full">
        {{-- contenido --}}
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 relative z-10 items-center h-full">
            <div class="text-white">
                <h1 class="text-white uppercase text-4xl mb-4 lg:mb-6 font-FuturaStdBook">lideres en la construcción</h1>
                <p class="text-xl mb-4 lg:mb-6">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum
                    laborum
                    reprehenderit ea fugit
                    consequuntur. Quasi deserunt neque iure nostrum dolores ullam impedit eos earum, corrupti nihil,
                    modi eveniet reiciendis omnis.</p>
                <button class="bg-brand_accent-500 hover:bg-brand_accent-600 py-2 px-8 uppercase font-bold text-sm">Ver
                    más</button>
            </div>
            <div class="flex justify-center">
                {{-- <h1 class="text-white text-3xl uppercase">cubo</h1> --}}


                {{-- hero right --}}
                {{-- <div class="hero-right">
                    <div class="contenedor-cubo">
                        <div class="cara">
                            <div class="item item-1">
                                <h4
                                    class="uppercase z-10 font-normal text-base md:text-xl text-white rotate-45 font-FuturaStdBook">
                                    construcción</h4>
                            </div>
                            <div class="item item-2">
                                <img class="px-2" src="/img/assets/logo_geo5_ bolivia.png" alt="" srcset="">
                            </div>
                            <div class="item item-3">
                                <h4
                                    class="uppercase z-10 font-normal text-base md:text-xl text-white rotate-45 font-FuturaStdBook">
                                    capacitación</h4>
                            </div>
                            <div class="item item-4">
                                <h4
                                    class="uppercase z-10 font-normal text-base md:text-xl text-white rotate-45 font-FuturaStdBook">
                                    cursos
                                </h4>
                            </div>
                            <div class="item item-5">
                                <h4
                                    class="uppercase z-10 font-normal text-base md:text-xl text-white rotate-45 font-FuturaStdBook">
                                    consultoría</h4>
                            </div>
                            <div class="item item-6">
                                <img class="px-2" src="/img/assets/fht_new_logo_white.svg" alt="" />
                            </div>
                        </div>
                    </div>
                </div> --}}

                {{-- lottie file --}}
                <div class="hero-right">
                    {{-- https://assets4.lottiefiles.com/private_files/lf30_6uhcq95s.json --}}
                    <lottie-player src="{{ asset('vendor/json/data.json') }}" background="transparent" speed="1"
                        style="width: 100%; height: 100%;" loop autoplay>
                    </lottie-player>
                </div>
            </div>
        </div>
    </div>
</header>
