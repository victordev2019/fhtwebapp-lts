@extends('adminlte::page')

@section('title', 'FHT | Actualizar Empresa')

@section('content_header')
    <div class="container">
        <h1>ACTUALIZAR INFORMACIÓN DE EMPRESA</h1>
    </div>
@stop

@section('content')
    @livewire('admin.web.company.edit-company', ['company' => $company])
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script></script>
@stop
