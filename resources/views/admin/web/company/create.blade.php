@extends('adminlte::page')

@section('title', 'FHT | Crear Empresa')

@section('content_header')
    <div class="container">
        <h1>INFORMACIÓN DE LA EMPRESA</h1>
    </div>
@stop

@section('content')
    @livewire('admin.web.company.create-company')
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        Livewire.on('guardar', async () => {
            const inputValue = '';

            const {
                value: category
            } = await Swal.fire({
                title: 'CREAR CATEGORÍA',
                input: 'text',
                inputLabel: 'Nueva categoría',
                inputValue: inputValue,
                inputPlaceholder: 'Ej. Diseño',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'El nombre de la categoría es requerido!'
                    }
                }
            })

            if (category) {
                Livewire.emitTo('admin.web.project.create-project', 'addCategory', category);
                Swal.fire(`La nueva categoría es ${category}`);
            }
        });
        // ********* temporaryUrl JS ************
        // document.getElementById('image').addEventListener('change', cambiarImagen);

        // function cambiarImagen(event) {
        //     var file = event.target.files[0];
        //     var reader = new FileReader();
        //     console.log('cargado!!!');
        //     reader.onload = (event) => {
        //         document.getElementById('picture').setAttribute('src', event.target.result);
        //     }
        //     reader.readAsDataURL(file);
        // }
    </script>
@stop
