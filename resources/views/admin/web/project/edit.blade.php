@extends('adminlte::page')

@section('title', 'FHT | Actualizar Proyecto')

@section('content_header')
    <div class="container">
        <h1>ACTUALIZAR INFORMACIÓN DEL PROYECTO</h1>
    </div>
@stop

@section('content')
    {{-- {{ $project }} --}}
    @livewire('admin.web.project.edit-project', ['project' => $project])
    {{-- @livewire('component', ['user' => $user], key($user->id)) --}}
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        Dropzone.options.myGreatDropzone = { // camelized version of the `id`
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            dictDefaultMessage: "Arrastre una imagen al recuadro max-size 5Mb.",
            acceptedFiles: "image/*",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            },
            complete: function(file) {
                this.removeFile(file);
                console.log('subido!!!');
            },
            queuecomplete: function(file) {
                Livewire.emit('refreshImage')
            }
        };
    </script>
@stop
