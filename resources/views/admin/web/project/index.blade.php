@extends('adminlte::page')

@section('title', 'FHT | Proyectos')

@section('content_header')
    <h1>Lista de proyectos</h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
    @livewire('admin.web.project.project-table')
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        Livewire.on('delete', projectId => {
            Swal.fire({
                title: 'Está segur@?',
                text: "La información del proyecto sera eliminada!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.web.project.project-table', 'destroy', projectId);
                    Swal.fire(
                        'Eliminado!',
                        'La información del proyecto ha sido eliminada',
                        'success'
                    )
                    console.log('Eliminando!!! ' + projectId);
                }
            })
        })
    </script>
@stop
