@extends('adminlte::page')

@section('title', 'FHT | Categorías')

@section('content_header')
    <h1>CATEGORÍAS PROYECTOS</h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
    @livewire('admin.web.project.category-table')
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        // ************ crear *************
        Livewire.on('nuevo', async () => {
            const inputValue = '';

            const {
                value: category
            } = await Swal.fire({
                title: 'CREAR CATEGORÍA',
                input: 'text',
                inputLabel: 'Nueva categoría',
                inputValue: inputValue,
                inputPlaceholder: 'Ej. Diseño',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'El nombre de la categoría es requerido!'
                    }
                }
            })

            if (category) {
                Livewire.emitTo('admin.web.project.category-table', 'store', category);
                Swal.fire(`La nueva categoría es ${category}`);
            }
        });
        // ************ update *************
        Livewire.on('edit', async (id, name) => {
            const inputValue = name;

            const {
                value: category
            } = await Swal.fire({
                title: 'ACTUALIZAR CATEGORÍA',
                input: 'text',
                inputLabel: 'Editar categoría',
                inputValue: inputValue,
                inputPlaceholder: 'Ej. Diseño',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'El nombre de la categoría es requerido!'
                    }
                }
            })

            if (category) {
                Livewire.emitTo('admin.web.project.category-table', 'update', id, category);
                Swal.fire(`La categoría ha sido actualizada`);
            }
        });
        // *********** deleted ***********
        Livewire.on('delete', categoryId => {
            Swal.fire({
                title: 'Está segur@?',
                text: "Los proyectos asociados a esta categoría también serán eliminadas!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.web.project.category-table', 'destroy', categoryId);
                    Swal.fire(
                        'Eliminado!',
                        'La información de la categiría ha sido eliminada',
                        'success'
                    )
                    console.log('Eliminando!!! ' + categoryId);
                }
            })
        })
    </script>
@stop
