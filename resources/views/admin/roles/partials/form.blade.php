{{-- @method('POST') --}}
{{-- @csrf --}}
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => 'Escriba un nombre']) !!}
    @error('name')
        <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<strong>Permisos</strong>
<br>
@foreach ($permissions as $permission)
    <div>
        <label>
            {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1', 'aria-label' => 'Checkbox for following text input']) !!}
            {{ $permission->name }}
        </label>
    </div>
@endforeach
@error('permissions')
    <small class="text-danger">
        <strong>{{ $message }}</strong>
    </small>
@enderror
<br>
