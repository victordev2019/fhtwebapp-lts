@extends('adminlte::page')

@section('title', 'FHT | Crear rol')

@section('content_header')
    <h1>CREAR ROL</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.roles.store']) !!}
            @include('admin.roles.partials.form')
            <div class="d-flex justify-content-end mt-4">
                <a href="{{ url()->previous() }}" class="btn btn-secondary mr-2">Cancelar</a>
                {!! Form::submit('Crear Rol', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
