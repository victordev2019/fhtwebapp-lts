@extends('adminlte::page')

@section('title', 'FHT | Editar rol')

@section('content_header')
    <h1>ACTUALIZAR ROL</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::model($role, ['route' => ['admin.roles.update', $role], 'method' => 'put']) !!}
            @include('admin.roles.partials.form')
            <div class="d-flex justify-content-end">
                <a href="{{ url()->previous() }}" class="btn btn-secondary mr-2">Cancelar</a>
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
