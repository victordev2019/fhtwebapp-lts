@extends('adminlte::page')

@section('title', 'FHT | Roles')

@section('content_header')
    <h1>LISTA DE ROLES</h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success" role="alert">
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @else
    @endif
    <div class="card">
        <div class="card-header">
            <a class="btn btn-primary float-right" href="{{ route('admin.roles.create') }}"><i
                    class="fas fa-plus-circle mr-2"></i>Crear
                Rol</a>
        </div>
        <div class="card-body">
            @if ($roles->count())
                <x-admin.web.table>
                    <x-slot name='head'>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th colspan="2">Opciones</th>
                    </x-slot>
                    <x-slot name='data'>
                        @forelse ($roles as $role)
                            <tr>
                                <th scope="row">{{ $role->id }}</th>
                                <td>{{ $role->name }}</td>
                                <td width="10px">
                                    @if ($role->id === 1)
                                        <a class="btn btn-default" href=""><i class="fas fa-edit"></i></a>
                                    @else
                                        <a class="btn btn-primary" href="{{ route('admin.roles.edit', $role) }}"><i
                                                class="fas fa-edit"></i></a>
                                    @endif
                                </td>
                                <td width="10px">
                                    <form action="{{ route('admin.roles.destroy', $role) }}" method="POST">
                                        @method('delete')
                                        @csrf
                                        @if ($role->id === 1)
                                            <button type="submit" class="btn btn-danger" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        @else
                                            <button type="submit" class="btn btn-danger"><i
                                                    class="fas fa-trash-alt"></i></button>
                                        @endif
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <div class="alert alert-warning" role="alert">
                                    No existen registros en la tabla!
                                </div>
                            </tr>
                        @endforelse
                    </x-slot>
                </x-admin.web.table>
            @else
                <div class="alert alert-info" role="alert">
                    <strong>Información!</strong> No se han encontrado registros en la base de datos.
                </div>
            @endif
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
