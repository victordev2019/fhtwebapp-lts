@props(['active'])

{{-- @php
$classes = $active ?? false ? 'inline-flex items-center px-1 pt-1 border-b-2 border-indigo-400 text-sm font-medium leading-5 text-gray-900 focus:outline-none focus:border-indigo-700 transition' : 'inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition';
@endphp --}}
@php
$classes = $active ?? false ? 'uppercase font-FuturaStdBook tracking-widest inline-flex items-center mt-2 px-1 py-6 border-b-4 border-white text-sm font-medium leading-5 text-white focus:outline-none transition' : 'uppercase font-FuturaStdLight tracking-widest inline-flex items-center mt-2 px-1 py-6 border-b-4 border-transparent text-sm font-light leading-5 text-brand_primary-100 hover:text-white hover:border-white focus:outline-none transition';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
