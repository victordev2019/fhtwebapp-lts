<?php

use App\Http\Controllers\Admin\Web\Company\CompanyController;
use App\Http\Controllers\Admin\Web\News\NewsController;
use App\Http\Controllers\Admin\Web\Project\CategoryProjectController;
use App\Http\Controllers\Admin\Web\Project\ProjectController;
use App\Http\Controllers\Admin\WebAdminController;
use App\Http\Livewire\Admin\Web\Project\CreateProject;
use App\Http\Livewire\Admin\Project\EditProject;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Project\ShowProjects;

// Route::get('/', ShowProjects::class)->name('webadmin.index');
Route::get('/', [WebAdminController::class, 'index'])->name('webadmin.index');
Route::controller(ProjectController::class)->group(function () {
    Route::get('project/list', 'index')->name('webadmin.project.index');
    Route::get('project/create', 'create')->name('webadmin.project.create');
    Route::get('project/{project}/edit', 'edit')->name('webadmin.project.edit');
    Route::post('project/{project}/files', 'files')->name('webadmin.project.files');
});
Route::controller(CategoryProjectController::class)->group(function () {
    Route::get('cateagory', 'index')->name('webadmin.project.category');
});
Route::controller(CompanyController::class)->group(function () {
    Route::get('company/list', 'index')->name('webadmin.company.index');
    Route::get('company/create', 'create')->name('webadmin.company.create');
    Route::get('company/{company}/edit', 'edit')->name('webadmin.company.edit');
});
Route::controller(NewsController::class)->group(function () {
    Route::get('news/list', 'index')->name('webadmin.news.index');
    Route::get('news/create', 'create')->name('webadmin.news.create');
    Route::get('news/{news}/edit', 'edit')->name('webadmin.news.edit');
});
// Route::get('projects/create', [ProjectController::class, 'create'])->name('webadmin.project.create');
// Route::get('projects/list', [ProjectController::class, 'index'])->name('webadmin.project.index');

// Route::get('projects/{project}/edit', EditProject::class)->name('webadmin.projects.edit');
// Route::get('projects/create', CreateProject::class)->name('webadmin.project.create');
// Route::post('projects/{project}/files', [ProjectController::class, 'files'])->name('webadmin.projects.files');
// Route::get('categories', [CategoryProjectController::class, 'index'])->name('webadmin.category.index');
