<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProyectosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class)->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
// ****************** RUTAS WEB*************************
Route::get('proyectos', ProyectosController::class)->name('proyectos');
Route::get('project/{project}', [ProjectController::class, 'show'])->name('project.show');
// ****************** RUTAS COURSOS*************************
Route::get('cursos', [CourseController::class, 'index'])->name('courses.index');
Route::get('cursos/{course}', function () {
    return 'Aqui se mostrará la informmación del curso.';
})->name('courses.show');
Route::get('contactos', function () {
    return view('contactos');
})->name('contactos');
Route::get('acercade', function () {
    return view('acercade');
})->name('acercade');
