<?php

use App\Models\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->string('client');
            $table->string('location');
            $table->string('service');
            $table->date('date')->default(now());
            $table->enum('status', [Project::BORRADOR, Project::PUBLICADO])->default(Project::PUBLICADO);

            $table->unsignedBigInteger('category_project_id');
            $table->foreign('category_project_id')->references('id')->on('category_projects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
