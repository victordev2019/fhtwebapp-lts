<?php

namespace Database\Factories;

use App\Models\CategoryProject;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        $category = CategoryProject::all()->random();
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'description' => $this->faker->text(),
            'client' => $this->faker->name(),
            'location' => $this->faker->city(),
            'service' => $this->faker->word(),
            'date' => $this->faker->date('Y-m-d', now()),
            'status' => rand(Project::BORRADOR, Project::PUBLICADO),
            'category_project_id' => $category->id
        ];
    }
}
