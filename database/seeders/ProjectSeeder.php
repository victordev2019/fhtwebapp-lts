<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Project;
use Database\Factories\ProjectFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::factory(20)->create()->each(function (Project $project) {
            Image::factory(2)->create([
                'imageable_id' => $project->id,
                'imageable_type' => Project::class
            ]);
        });
    }
}
