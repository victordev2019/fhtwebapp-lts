<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Victor Avalos Torrez',
            'email' => 'tigervat10@outlook.com',
            'password' => bcrypt('12345678')
        ])->assignRole(['Admin', 'Webadm']);
        User::factory(49)->create();
    }
}
