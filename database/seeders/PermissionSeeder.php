<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'Crear Proyecto'
        ]);
        Permission::create([
            'name' => 'Editar Proyecto'
        ]);
        Permission::create([
            'name' => 'Eliminar Proyecto'
        ]);
        Permission::create([
            'name' => 'Ver Proyecto'
        ]);
    }
}
