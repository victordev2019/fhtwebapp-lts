<?php

namespace Database\Seeders;

use App\Models\CategoryProject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoryProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = ['Cosntrucción', 'Consultoría', 'Ejecución'];
        foreach ($cats as $cat) {
            # code...
            CategoryProject::create([
                'name' => $cat
            ]);
        }
    }
}
